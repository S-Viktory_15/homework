/*Створити конструктор Animal та розширюючі його конструктори Dog, Cat, Horse.
Конструктор Animal містить змінні food, location і методи makeNoise, eat, sleep. Метод makeNoise, наприклад, 
може виводити на консоль "Така тварина спить".
Dog, Cat, Horse перевизначають методи makeNoise, eat.
Додайте змінні до конструкторів Dog, Cat, Horse, що характеризують лише цих тварин.
Створіть конструктор Ветеринар, у якому визначте метод який виводить в консоль treatAnimal(Animal animal).
 Нехай цей метод роздруковує food і location тварини, що прийшла на прийом.
У методі main створіть масив типу Animal, в який запишіть тварин всіх типів, що є у вас. У циклі надсилайте 
їх на прийом до ветеринара.*/

function Doc() {

}
Doc.prototype.treatAnimal = function (animal) {
    console.log(Object.values(animal))
    

}

function Animal(food, location) {
    this.food = "Ковбасу";
    this.location = "Земля";

}
Animal.prototype.makeNoise = function () {
    console.log(`${this.name} Така тварина спить`)

}
Animal.prototype.eat = function () {
    console.log(`${this.name} Така тварина їсть`)

}
Animal.prototype.sleep = function () {
    console.log(`Тваринка спить`)

}


function Dog(name, food, location) {
    this.name = name;
    this.food = food;
    this.location = location;
    

}
Dog.prototype = new Animal()
Dog.prototype.makeNoise = function () {
    console.log(`${this.name} спить 10 годин`)
}
Dog.prototype.eat = function () {
    console.log(`${this.name} їсть корм`)

}


function Cat(name, food, location) {
    this.name = name;
    this.food = food;
    this.location = location;

}
Cat.prototype = new Animal()
Cat.prototype.makeNoise = function () {
    console.log(`${this.name} спить 5 годин`)
}
Cat.prototype.eat = function () {
    console.log(`${this.name} п'є молоко`)

}


function Horse(name,  food, location) {
    this.name = name;
    this.food = food;
    this.location = location;

}
Horse.prototype = new Animal()
Horse.prototype.makeNoise = function () {
    console.log(`${this.name} спить 12 годин`)
}
Horse.prototype.eat = function () {
    console.log(`${this.name} їсть сіно`)

}


function main() {
    const animals = [new Dog("Bob", "meet", "Kharkiv"), new Cat("Lucky", "milk", "Odessa"), new Horse("Donna", "hay", "Kiev")]

    animals.forEach((element) => {
        new Doc ().treatAnimal(element)
        
    });
    for(let key in animals) {
        const a = animals[key];
        a.makeNoise();
    };
    for(let key in animals) {
        const a = animals[key];
        a.eat();
    };

    
}


main();








