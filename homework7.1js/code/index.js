/*Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. 
Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", 
"Поворот праворуч" або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію про 
автомобіль, її водія і двигуна.

Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.
https://storage.googleapis.com/www.examclouds.com/oop/car-ierarchy.png*/


class Car{
    constructor(brand, classCar, weight, driver, engine) {
        this.brand = brand
        this.classCar = classCar
        this.weight = weight
        this.driver = driver
        this.engine = engine
       
    }


    start() {
        console.log("Поїхали")
    }
    stop() {
        console.log("Зупиняємося")
    }
    turnRight() {
        console.log("Поворот праворуч")
    }
    turnLeft() {
        console.log("Поворот ліворуч")
    }
    toString() {
        console.log(`Автомобіль: марка - ${this.brand}, клас - ${this.classCar}, вага - ${this.weight} кг. Водій: ${this.driver}. Двигун: ${this.engine} `)
    }


}
class Engine {
    constructor(power, company) {
        this.power = power
        this.company = company


    }
    toString(){
        return( `потужність - ${this.power} к.с., виробник - ${this.company}`);
    }
}
class Driver{
    constructor(pip, experience) {
        this.pip = pip
        this.drivingExperience = experience


    }
toString(){
    return(`ПІП - ${this.pip}, стаж водіння - ${this.drivingExperience} років`)

}

}
class Lorry extends Car {
    constructor (brand, classCar, weight, carrying, driver, engine) {
        super(brand, classCar, weight, driver, engine)

        this.carrying = carrying
    }
    toString() {
        console.log(`Автомобіль: марка - ${this.brand}, клас - ${this.classCar}, вага - ${this.weight}кг, вантажопідйомність кузова - ${this.carrying}кг. Водій: ${this.driver}. Двигун: ${this.engine} `)
    }
  }

  class SportCar extends Car {
    constructor (brand, classCar, weight, speed, driver, engine) {
        super(brand, classCar, weight, driver, engine)

        this.speed =speed

    }
    toString() {
        console.log(`Автомобіль: марка - ${this.brand}, клас - ${this.classCar}, вага - ${this.weight}кг, гранична швидкість - ${this.speed} км/год. Водій: ${this.driver}. Двигун: ${this.engine} `)
    }
  } 
const D = new Engine( 117, "Nissan");
const C = new Driver("Іванов Іван Іванович", 10)
const user = new Car ("NISSAN JUKE", "SUV В-класса", 2000, C, D);

const D1 = new Engine( 210, "КАМАЗ");
const C1 = new Driver("Сергіїв Сергій Сергійович", 7)
const user2 = new Lorry ("КамАЗ-4310", "вантажний автомобіль", 7080, 5000, C1, D1);


const D2 = new Engine(1000, "Ferrari" );
const C2 = new Driver("Петров Петро Петрович", 12)
const user3 = new SportCar ("Ferrari SF90 Stradale", "спортивний автомобіль", 1570, 340, C2, D2);






user.toString();
user2.toString();
user3.toString();
user.start();
user.stop()
user.turnRight();
user.turnLeft();

user3.start();







