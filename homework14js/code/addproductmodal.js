import { showForm } from "./functions.js";

//Знаходимо контейнер та тіло модульного вікна, кнопку відкривання та закривання модульного вікна,
//  та select (випадаючий список для вибору категорії).
const modal = document.querySelector(".container-modal");
const modalBody = document.querySelector(".modal-body");
const addProduct = document.querySelector("#btn-add-product");
const modalClose = document.getElementById("modal-close");
const productSelect = document.getElementById("product-select");

// На кнопку відкривання модульного вікна "Додати щось нове" навішуємо подію click, при натиску на кнопку на 
// контейнері модального вікна видаляємо клас hide, який приховував модальне вікно. 
addProduct.addEventListener("click", () => {
    modal.classList.remove("hide");
})

// На кнопку закривання модульного вікна навішуємо подію click, при натиску на кнопку на 
// контейнері модального вікна додаємо клас hide, який приховує модальне вікно. 
modalClose.addEventListener("click", () => {
    modal.classList.add("hide")
})

// На productSelect (вирадаючий список) навішумо подію change. Якщо відбудеться зміна в  productSelect, то
// викликається функція showForm (яка імпортована з файла functions.js) і в якості аргументу їй буде
// передано value в нижньому реєстрі інціатора виклику події.
productSelect.addEventListener("change", (e) => {
    //{ id, name, qantity, price, status, date, description} - за допомогою ксласу.
    showForm(e.target.value.toLowerCase())
})