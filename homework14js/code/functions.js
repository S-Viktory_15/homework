// Знаходимо кнопку "Додати", в модальному вікні "Додати щось нове".

const buttonSend = document.querySelector("#submit");

// Експортуємо функцію до модульного файлу addproductmodal.js. Функція приймає аргументу та робить її
// перевірку. В залежності від того, чому дорівнюватиме аргумета productType, буде викликатись функція 
// createStoreForm або createRestoranForm, або createVideoForm, яку після деструкторизації додаємо до div з
// класом product-body (productBody), розміщеному в модальному вікні (додавання нового продукту), тобто
//  виводимо форми для введення інфомації в модальне вікно. Також робимо 
// очищення productBody, щоб при виборі іншої категорії не лищались дані попередньої.
// Для того, щоб при виборі іншої категорії зробити кнопку "Додати" знову не активною, викликаємо функцію 
// sendBtnDisabled. При натиску на кнопку "Додати" викликаємо функцію sendData та закриваємо модальне вікно.
export function showForm(productType = "магазин") {
    sendBtnDisabled()
    const productBody = document.querySelector(".product-body");
    productBody.innerHTML = "";
    // product-body 
    if (productType === "магазин") {
        productBody.append(...createStoreForm());
       
    } else if (productType === "ресторани") {
        productBody.append(...createRestoranForm());
      

    } else if (productType === "відео хостинг") {
        productBody.append(...createVideoForm())
      
    }
    buttonSend.addEventListener("click", () => {
        sendData(productType);
        document.querySelector(".container-modal").classList.add("hide");
    });

}
// Функція, яка повертає масив зі створеними елементами форми для магазину. Тричі викликаємо функцію 
// createElementForm (т. як створюємо 3 label, input і div) та передаємо їй відповідні аргумкним для заповнення.
function createStoreForm() {
    return [
        createElementForm(
            "Введіть назву продукта",
            "text",
            "product-name",
            "текст має бути Українською"),
        createElementForm(
            "Введіть ціну",
            "number",
            "product-price",
            "тут мають бути лише числа"),
        createElementForm(
            "Опис товару",
            undefined,
            "product-description",
            "текст має бути Українською")
    ]
}

//Функція, яка повертає масив зі створеними елементами форми для ресторану, аналогічно як для магазину.
function createRestoranForm() {
    return [
        createElementForm(
            "Введіть назву продукта",
            "text",
            "product-name",
            "текст має бути Українською"),
        createElementForm(
            "Введіть ціну",
            "number",
            "product-price",
            "тут мають бути лише числа"),
        createElementForm(
            "Опис товару",
            undefined,
            "product-description",
            "текст має бути Українською")
    ]
}

//Функція, яка повертає масив зі створеними елементами форми для відео хостингу, аналогічно як для магазину
// та ресторану, але функція createVideoForm викликалась лише 2 рази (т. як створюємо 2 label, input і div).
function createVideoForm() {
    return [
        createElementForm(
            "Назва відео",
            "text",
            "video-name",
            "текст має бути Українською"),
        createElementForm(
            "Посилання на відео",
            "url",
            "video-url",
            "Вкажіть в форматі https://name.org")
    ]
}

// Універсальна функція для створення форми, для вводу даних користувачем. Створюємо елементи форми:
// label, input, div(для виводу помилки при невірному вводі) та parent (обгортка для вищевказаних елементів).
// В якості аргументів записуємо placeholder для label, type та classInput для input, errorText для error 
// та функцію-оброблювальник для подій, та відповідно присвоюємо їх відповідним елементам. Додаємо клас input
// для parent та класи error (при невірному введенні інформації ) і hide (при вірному введенні інформації) 
// для div(error). Поміщаємо label, input і error в parent.
// На input навішуємо подію focus, яка при фокусуванні на input додає до label клас none-placeholder, який
// переміщує placeholder label-а над input.
// На input навішуємо подію blur, яка при втраті фокусу, за умови, що value  на input-і є пустим рядком,
// видаляє з label клас none-placeholder та відповідно переміщує placeholder label-а в input.
// Створюємо властивість input.validate, якій за замовчуванням присвоюємо значення false.
// При зміні на input викликаємо функцію validate, яка в якості аргументів приймає 
// value (дані введені користувачем) та classList (клас input-а) ініціатора виклику,
// error (div, який показує помилку) та сам input.
function createElementForm(placeholder = "", type = "text", classInput = "", errorText = "Помилка", eventInput = () => { }) {
    const parent = document.createElement("div"),
        label = document.createElement("label"),
        input = document.createElement("input"),
        error = document.createElement("div");

    parent.classList.add("input")
    label.innerText = placeholder;
    input.type = type;
    input.validate = false;
    input.classList.add(classInput);
    error.classList.add("error", "hide");
    error.innerText = errorText;
    input.addEventListener("change", eventInput);
    input.addEventListener("input", (e) => {
        validate(e.target.value, e.target.classList.value, error, input);
    });
    input.addEventListener("focus", () => {
        label.classList.add("none-placeholder")
    })
    input.addEventListener("blur", () => {
        if (!input.value) {
            label.classList.remove("none-placeholder")
        }
    })

    parent.append(label, input, error);

    return parent
}

//Функція для створення Id продукту. Створюємо 3 змінні: symbols, яка містить всі можливі символи потрібні
// для генерації Id, passLength - кількість символів, що має містити  Id та string - пустий рядок в який
// за допомогою цикла та методу вибору випадкового значення створюється Id.
function createId() {
    let symbols = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890";
    let passLength = 8;

    let string = "";

    for (let i = 0; i < passLength; i++) {
        let n = Math.floor(Math.random() * symbols.length);
        string += symbols[n];
    }
    return string;
}

// Створюємо класи для кожної категорії з відповідними властивостями.
class StoreProduct {
    constructor(name, price, description, date) {
        this.id = createId();
        this.name = name;
        this.price = price;
        this.status = false;
        this.date = date;
        this.description = description;
        this.category = this.constructor.name;
        this.quantity = "0";
    }
}
class RestProduct {
    constructor(name, price, description, date) {
        this.id = createId();
        this.name = name;
        this.price = price;
        this.status = false;
        this.date = date;
        this.description = description;
        this.category = this.constructor.name;
        this.quantity = "0";
    }
}
class VideoProduct {
    constructor(name, url, date) {
        this.id = createId();
        this.name = name;
        this.url = url;
        this.date = date;
        this.category = this.constructor.name;
    }
}

// Створюємо масиви для кожної категорії, для перенесення інформації в localStorage.
let storeProducts = [];
let restProducts = [];
let videoProducts = [];


// Функція передачі данних в localStorage. Сворюємо змінну date (дата та час в заданому форматі). 
// Далі перевіряємо якій з категорій (магазин, ресторан чи відео хостинг) ідентична аргумента type.
// Шукаємо значення введені користувачем для кожної категорії, створюємо нові класи StoreProduct,
// RestProduct та  VideoProduct, та в якості аргументів передаємо знайдені значення. Створені класи додаємо до
// відповідних масивів (192 - 194 рядок) та додаємо їх до localStorage. Робимо перевірку якщо в localStorage
// вже існують дані за заданим ключем, то дані у вищевказаних масивах перезаписати. 
// Чистимо значення полів вводу для подального вводу та викликаємо функцію sendBtnDisabled для дезактивації
// кнопки "Додати".
function sendData(type) {
    let date = new Date();
    date = date.toLocaleString().split(",")[0].split(".").reverse().join(".") + date.toLocaleString().split(",")[1];
    if (type === "магазин") {
        const name = document.querySelector(".product-name").value;
        const price = document.querySelector(".product-price").value;
        const desc = document.querySelector(".product-description").value;
        if (localStorage.storeProducts) {
            storeProducts = JSON.parse(localStorage.getItem("storeProducts"));
        }
        storeProducts.push(new StoreProduct(name, price, desc, date));
        localStorage.setItem("storeProducts", JSON.stringify(storeProducts));
        document.querySelector(".product-name").value = ""
        document.querySelector(".product-price").value = ""
        document.querySelector(".product-description").value = ""
        sendBtnDisabled()
    }
    else if (type === "ресторани") {
        const name = document.querySelector(".product-name").value;
        const price = document.querySelector(".product-price").value;
        const desc = document.querySelector(".product-description").value;
        if (localStorage.restProducts) {
            restProducts = JSON.parse(localStorage.getItem("restProducts"));
        }
        restProducts.push(new RestProduct(name, price, desc, date));
        localStorage.setItem("restProducts", JSON.stringify(restProducts));
        document.querySelector(".product-name").value = ""
        document.querySelector(".product-price").value = ""
        document.querySelector(".product-description").value = ""
        sendBtnDisabled()
    }
    else if (type === "відео хостинг") {
        const name = document.querySelector(".video-name").value;
        const url = document.querySelector(".video-url").value;
        if (localStorage.videoProducts) {
            videoProducts = JSON.parse(localStorage.getItem("videoProducts"));
        }
        videoProducts.push(new VideoProduct(name, url, date));
        localStorage.setItem("videoProducts", JSON.stringify(videoProducts));
        document.querySelector(".video-name").value = ""
        document.querySelector(".video-url").value = ""
        sendBtnDisabled()
    }
}

// Функція валідації даних введених коритистувачем. В якості аргументів передаємо value (дані введені користувачем), 
// classList (клас input-а), error (div, який показує помилку) та сам input. Далі робимо перевірку чи містить
// клас в назві name, price,  description або url та згідно неї створюємо patern (умови, яким повинні відповідати 
// введені дані) для кожного input-а відповідного класу. Потім за допомогою метода test перевіряємо чи 
// відповідають введені дані заданим умовам. Якщо так, то до error додаємо клас hide (div прихований), а властивість 
// input.validate переводимо в стан true (після виклику функції buttonShow кнопка "Додати" буде активною), якщо
// ні, то з error видаляється клас hide (стає видно div з помилкою та input підсвічується червоним), а 
// input.validate переводимо в стан false (після виклику функції buttonShow кнопка "Додати" буде неактивною).
 
const validate = (value, classList, error, input) => {
    if (classList.includes("name")) {
        const patern = /^[а-яіїєґ0-9- ]{3,20}$/i;
        if (patern.test(value)) {
            error.classList.add("hide");
            input.validate = true;
        }
        else {
            input.validate = false
            error.classList.remove("hide");
        };
    }
    if (classList.includes("price")) {
        const patern = /^[0-9.]{1,6}$/i;
        if (patern.test(value)) {
            error.classList.add("hide");
            input.validate = true;
        }
        else {
            error.classList.remove("hide");
            input.validate = false
        }
    }
    if (classList.includes("description")) {
        const patern = /^[а-яіїєґ0-9- ]{5,}$/i;
        if (patern.test(value)) {
            error.classList.add("hide");
            input.validate = true;
           
        }
        else {
            error.classList.remove("hide");
            input.validate = false
        }
    }
    if (classList.includes("url")) {
        const patern = /^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i;
        if (patern.test(value)) {
            error.classList.add("hide");
            input.validate = true;
        }
        else {
            error.classList.remove("hide");
            input.validate = false
        }
    }
    buttonShow()
}


// Функція активації кнопки. Шукаємо всі інпути модального вікна. Створюємо змінну rez, яка перевіряє чи
// відповідають всі елементи (inputs) масива заданій умові (inputs.validate === true). Далі робимо перевірку -
// якщо відповідають умові, то кнопка "Додати" - активна, якщо ні, то неактивна. 
function buttonShow() {
    const [...inputs] = document.querySelectorAll("input");
    const rez = inputs.every(a => {
        return a.validate === true;
    });

    if(rez){
        buttonSend.disabled = false;
    }else{
        buttonSend.disabled = true;
    }
}

// Функція, яка  робить не активною кнопку "Додати" в модальному вікні для додавання нового продукту.
const sendBtnDisabled = () => {
    buttonSend.disabled = true;
}



