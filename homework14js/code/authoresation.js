// Функція, яка перевіряє чи авторизований користувач. Робимо перевірку - якщо localStorage містить дані 
// авторизації - не робити нічого, якщо  location не містить "authorization" - відкрити 
// сторінку /authorization/index.html.
function isAuth() {
    if (localStorage.isAuth) {
        return
    } else if (!location.pathname.includes("authorization")) {
        location = "/authorization"
    }
}
// В блок try помістили частину коду на якому вибивало помилку, для подальшої коректної роботи коду.
try {
    console.log((new Date().toLocaleDateString("en", { weekday: "long" }) + new Date().getHours()).toLocaleLowerCase());
    console.log(new Date().toLocaleDateString("uk", { weekday: "long" }).toLocaleLowerCase() + new Date().getMinutes());

    //Функція, яка перевіряє чи вірно ввів користувач дані при авторизації. Створили змінну loginData, 
    // в якій визначили день тижня на англійській мові та годину, та перевели ці дані в нижній реєстр. 
    // Створили змінну passwordData в якій визначили день тижня на українській мові
    //  та хвилини, та перевели ці дані в нижній реєстр.  Далі зробили перевірку - 
    //якщо введені користувачем значення логіну і паролю ідентичні  loginData і passwordData, то авторизація в
    //localStorage вірна, і на inputLogin та inputPassword видалили клас error, та зробили переадресацію на головну
    //сторінку,  якщо введені користувачем значення логіну і паролю не ідентичні  loginData і passwordData,
    //  то на inputLogin та inputPassword додати клас error (підсвітити червоним кольором).
    function Auth() {
        const loginData = (new Date().toLocaleDateString("en", { weekday: "long" }) + new Date().getHours()).toLocaleLowerCase();
        const passwordData = new Date().toLocaleDateString("uk", { weekday: "long" }).toLocaleLowerCase() + new Date().getMinutes();

        if (inputLogin.value === loginData && inputPassword.value === passwordData) {
            localStorage.isAuth = true;
            inputLogin.classList.remove("error");
            inputPassword.classList.remove("error");
            location = "/"
        } else {
            inputLogin.classList.add("error")
            inputPassword.classList.add("error")
        }
    }

    // Знайшли кнопку "Увійти" та поля для вводу даних логіну та паролю inputLogin та inputPassword.
    const btn = document.querySelector("#btn");
    const inputLogin = document.querySelector("[data-type='login']");
    const inputPassword = document.querySelector("[data-type='password']");

    // На поля вводу логіну та паролю навішуємо подію change, і якщо відбулись зміни на полях, то викликається
    //функція btnShow.
    inputLogin.addEventListener("change", () => {
        btnShow();
    })

    inputPassword.addEventListener("change", () => {
        btnShow();
    })

    // На кнопку "Увійти" навішуємо подію click, і якщо відбулось натискання кнопки, то викликається
    //функція Auth.
    btn.addEventListener("click", () => {
        Auth()
    })

    // Функція, яка робить активною кнопку "Увійти". Робимо перевірку - якщо поля вводу логіна та паролю не є
    // пустими рядками, то на кнопці "Увійти " робимо не активним атрибут disabled, в іншому випадку атрибут 
    // disabled залишається активним.
    function btnShow() {
        if (inputLogin.value !== "" && inputPassword.value !== "") {
            btn.disabled = false;
        } else {
            btn.disabled = true;
        }
    }
// якщо виникла помилка, то її нікуди не виводити.
} catch (e) {

}

//Експортуємо функцію isAuth до модульного файлу.
export { isAuth }