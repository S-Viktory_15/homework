// Знаходимо тіло таблиці та чистимо його.
// Робимо перевірку - якщо localStorage  за ключем storeProducts пустий  
//або містить пустий масив,то знаходимо контейнер за Id box-show та додаємо перед його закриваючим тегом div 
// з текстом "Поки немає даних ❌". 
// Якщо localStorage за ключем storeProducts не пустий, то дістаємо дані в змінну products та перебираємо їх
// за допомогою forEach, викликаючи на кожному елементі функцію createTableElement.

export function store() {
    const table = document.querySelector("tbody");
    table.innerHTML = "";

    if (!localStorage.storeProducts || localStorage.storeProducts === "[]") {
        document.getElementById("box-show").insertAdjacentHTML("beforeend", `<div class="box-none">Поки немає даних ❌</div>`)
        return;
    }
    const products = JSON.parse(localStorage.storeProducts);
  

    products.forEach((product, index) => {
        createTableElement(index + 1, product, "line", editEvent, deleteEvent, table);
    });
    // Перевірка стореджа
}

// Функція створює елементи таблиці. 
// Створюємо обєкт product з даними необхідними для заповнення таблиці. Створюємо рядок таблиці, кнопки з текстом
// "Редагувати" та "Видалити", додаємо кнопкам курсор "Руку". На кнопки навішуємо подію click, при натиску на
// кнопки викликаються функції editEvent та deleteEvent відповідно, в якості аргументи передаємо їм product.
// Далі робимо перевірку адресного рядка location на наявність у нього в назві  store, restoran чи video та
// відповідно до цього створює масив elements, елементами якого є виклик функцій newTd з аргументами, які 
// відповідають ячейкам таблиць відповідних категорій. Потім до рядка додаємо розкритий масив elements, а рядок 
// додаємо до тіла таблиці.

function createTableElement(number, product, className, editEvent, deleteEvent, table) {
    const { name, quantity, price, status, description, date, category, url } = product;
    const tr = document.createElement("tr");

    const editButton = document.createElement("button");
    editButton.innerText = "Редагувати";
    editButton.style.cursor = "pointer";
    editButton.addEventListener("click", () => {
        editEvent(product);
    });

    const delButton = document.createElement("button");
    delButton.innerText = "Видалити";
    delButton.style.cursor = "pointer";
    delButton.addEventListener("click", () => {
        deleteEvent(product);
    });

    if (window.location.pathname.includes("store")) {
        const elements = [
            newTd(number),
            newTd(name),
            newTd(quantity),
            newTd(price),
            newTd(editButton),
            newTd(status ? "✅" : "❌"),
            newTd(date),
            newTd(delButton)
        ]
        tr.append(...elements);
    }
    else if (window.location.pathname.includes("restoran")) {
        const elements = [
            newTd(number),
            newTd(name),
            newTd(quantity),
            newTd(price),
            newTd(editButton),
            newTd(status ? "✅" : "❌"),
            newTd(date),
            newTd(delButton)
        ]
        tr.append(...elements);
    }
    else if (window.location.pathname.includes("video")) {
        const elements = [
            newTd(number),
            newTd(name),
            newTd(date),
            newTd(url),
            newTd(editButton),
            newTd(delButton)
        ]
        tr.append(...elements);
    }
    table.append(tr);
}

// Функція, яка створює ячейки таблиці. Створюємо ячейки таблиці td та робимо перевірку - якщо аргумента не 
// прийшла, то не робимо нічого, якщо назва аргумента ідентична button, то в ячейку додаємо кнопку, в іншому
// випадку до ячейки додаємо текст.
function newTd(props) {

    if (!props) return;
    const td = document.createElement("td");
    if (props.localName === "button") {
        td.append(props);
    }
    else {
        td.innerText = props;
    }
    return td;
}


// Функція видалення продукту. Створюємо змінну allProducts в яку дістаємо дані з localStorage по ключу 
// storeProducts. Далі за допомогою методу масиву findIndex знаходимо в масиві allProducts елемент, id якого
// ідентичний id, який був переданий в якості аргументу, та  за допомогою методу масиву splice видаляємо його.
// В localStorage за ключем storeProducts перезаписуємо дані (оновлений allProducts) та викликаємо функцію store. 
function deleteEvent(product) {
    const { id } = product;
    const allProducts = JSON.parse(localStorage.storeProducts);

    allProducts.splice(allProducts.findIndex(el => id === el.id), 1)
    //allProducts = allProducts.length === 0? undefined : allProducts
    localStorage.storeProducts = JSON.stringify(allProducts);
    store()
}

// Функція редагування даних в таблиці. Створюємо змінну allProducts в яку дістаємо дані з localStorage по ключу 
// storeProducts. Далі за допомогою методу масиву findIndex знаходимо в масиві allProducts елемент, id якого
// ідентичний id, який було передано в якості аргументу, та  за допомогою методу масиву splice видаляємо його.
// Робимо деструкторизацію отриманого масиву. Знаходимо модальне вікно та кнопку закриття модального вікна для 
// редагування даних в таблиці. З модального вікна видаляємо клас hide (робимо його видимим), а на кнопку закриття
// навішуємо подію click - при натиску на неї до модального вікна додати клас hide (робимо його невидимим).
// Далі викликаємо функцію showPropertyProduct, в якості аргументу передаємо oldObj.
function editEvent(product) {
    //name, quantity, price, status, description, date, category, url 
    let {id} = product;
    const allProducts = JSON.parse(localStorage.storeProducts);
    const [oldObj] = allProducts.splice(allProducts.findIndex(el => id === el.id), 1);
    const modal = document.querySelector(".container-modal");
    const close = document.querySelector("#modal-close")

    modal.classList.remove("hide");
    close.addEventListener("click", () => {
        modal.classList.add("hide");
    })
    showPropertyProduct(oldObj)
}

// Відображення форми для редагування таблиці. Знаходимо форму за Id edit, в яку будемо виводити інформацію. 
// В змінну props за допомогою медода Object.entries дістаємо в масив з масивів ключі та значення обєкта,  
//  що прийшов у вигляді аргументи. За допомогою метода map перебираємо данний масив, розкриваючи його та на 
// кожному елементі викликаємо функцію createPropertyElement, якій в якості першого аргумента передаємо ключ,
//а другого значення. Отримані дані поміщаємо в форму edit.
function showPropertyProduct (p) {
    const edit = document.getElementById("edit");
    const props = Object.entries(p)
    console.log(props);
    const formData =  props.map(([a1, a2])=>{
      return  createPropertyElement(a1, a2)
    })

    edit.append(...formData)
}

// Створення форми для редагування даних таблиці. Створюємо змінну id (рандомне число), div, label та input.
// Звязуємо між собою label та input присвоюючи їм атрибути for та id, які дорівнюватимуть const id. 
// Додаємо для input value,  а для label текст, які приходять в якості атрибутів. label та input поміщаємо 
// всередину div. 
function createPropertyElement (name, value, fnEvent) {
    const id = Math.floor(Math.random()*1000000);
    const div = document.createElement("div");
    const label = document.createElement("label");
    const input = document.createElement("input");
    label.setAttribute("for", id);
    input.id = id;
    input.value = value;
    label.innerText = name;
    div.append(label, input);
    return div
}
