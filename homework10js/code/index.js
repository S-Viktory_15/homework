/*
* У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма 
чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так 
і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - 
це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. 
Повторне натискання `MRC` має очищати пам'ять.
*/

window.addEventListener('DOMContentLoaded', () => {
    const one = document.querySelector(".keys");
    const result = document.querySelector("#display");
    let a = "";
    let b = "";
    let oper = "";
    let d = "";
    let finish = false;

    function clearAll() {
        a = "";
        b = "";
        oper = "";
        finish = false;
        result.value = 0;


    }

    one.addEventListener("click", function (event) {
        document.querySelector("#eq").disabled = false;


        if (!event.target.classList.contains("button")) {
            return;
        }
        if (event.target.classList.contains("C")) {
            clearAll();
            return;
        }

        const key = event.target.value;

        if (event.target.classList.contains("black")) {

            if (b === '' && oper === '') {
                a += key;
                result.value = a;
                


            }
            else if (!a == '' && !b == '' && finish) {
                b = key;
                finish = false;
                result.value = b;

            }
            else {
                b += key;
                result.value = b;
                


            }

        }


        if (event.target.classList.contains("pink")) {
          
              oper = key;
            
                
            }

        if (event.target.classList.contains("orange")) {
            if (key === '') {
                if (b === '') b = a;

            }
            switch (oper) {
                case "+": a = (+a) + (+b);
                    break;
                case "*": a = a * b;
                    break;
                case "-": a = a - b;
                    break;
                case "/": a = a / b;
                    break;
            }
            finish = true;
            result.value = a;


        }
        if(event.target.classList.contains("m1")){
           
            d = a;
            clearAll();
            result.value = d;
            document.querySelector(".m").classList.remove("hide");
            document.querySelector(".m").classList.add("m");
            
        }
        
        if(event.target.classList.contains("m2")){
            document.querySelector(".m").classList.toggle("hide");
            d ="";
            clearAll();
        }
        if(event.target.classList.contains("m3")){
          a=d;
            result.value = a;
            
        }

    })
})