if (!Array.isArray(data)) {
    throw new Error("Ми отримали щось інше")
}
class Card {
    constructor(name, img, price, description) {
        this.name = name
        this.img = img
        this.productPrice = price
        this.productDescription = description
    }
    bootstrapCard() {
        const card = `<div class="card" style="width: 18rem;">
        <img src="${this.img}" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">${this.name} title</h5>
          <p class="card-text">${this.productPrice} </p>
          <p class="card-text">${this.productDescription} </p>
          <a href="#" class="btn btn-primary">Go somewhere</a>
        </div>
      </div>`;
      document.getElementById("card").innerHTML += card;
      

    }


}


data.forEach((obj) => {
    const row = new Card (obj.title, obj.image, obj.price, obj.description);
    row.bootstrapCard()

    
})




