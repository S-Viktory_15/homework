const mas = [];
data.forEach((obj) => {
    createCard(obj);
});
function createCard(obj) {
    const u = document.createElement("div");
    u.classList.add("card");
    const card = `<div class = "description">
        <div class="pic"><img src="${obj.image}" alt=""></div>
        <div class="product-name">${obj.title}</div>
        <div class="rating">
            <svg width="150" height="25" viewBox="0 0 150 25" fill="none"
                xmlns="http://www.w3.org/2000/svg">
                <path
                    d="M12.1795 0L15.7587 7.25312L23.7629 8.41582L17.9708 14.0612L19.3384 22.0329L12.1795 18.2688L5.02056 22.0329L6.38819 14.0612L0.596107 8.41582L8.60027 7.25312L12.1795 0Z"
                    fill="#FFCC48" />
                <path
                    d="M43.5896 0L47.1689 7.25312L55.173 8.41582L49.3809 14.0612L50.7486 22.0329L43.5896 18.2688L36.4307 22.0329L37.7983 14.0612L32.0063 8.41582L40.0104 7.25312L43.5896 0Z"
                    fill="#FFCC48" />
                <path
                    d="M75 0L78.5793 7.25312L86.5834 8.41582L80.7913 14.0612L82.159 22.0329L75 18.2688L67.8411 22.0329L69.2087 14.0612L63.4167 8.41582L71.4208 7.25312L75 0Z"
                    fill="#FFCC48" />
                <path
                    d="M106.41 0L109.989 7.25312L117.994 8.41582L112.201 14.0612L113.569 22.0329L106.41 18.2688L99.2513 22.0329L100.619 14.0612L94.8268 8.41582L102.831 7.25312L106.41 0Z"
                    fill="#FFCC48" />
                <path
                    d="M137.821 0L141.4 7.25312L149.404 8.41582L143.612 14.0612L144.98 22.0329L137.821 18.2688L130.662 22.0329L132.029 14.0612L126.237 8.41582L134.241 7.25312L137.821 0Z"
                    fill="#FFCC48" />
            </svg>
        </div>
        <div class="price">${obj.price} грн</div>
        </div>`;
    const productColour = document.createElement("div");
    productColour.classList.add("product-color");
    const colour = Object.values(obj.color);
    colour.forEach((el) => {
        if (el !== "") {
            const inpColour = document.createElement("input");
            inpColour.setAttribute("type", "button");
            inpColour.className = `${el}`;
            productColour.prepend(inpColour);
        }
        else {
            return;
        }
    });
    const productSize = document.createElement("div");
    productSize.classList.add("size1");
    const size = Object.values(obj.size);
    size.forEach((el) => {
        if (el !== "") {
            const inpSize = document.createElement("input");
            inpSize.setAttribute("type", "button");
            inpSize.value = `${el}`;
            productSize.prepend(inpSize);
        }
        else {
            return;
        }
    })
    const divBtn = document.createElement("div");
    divBtn.classList.add("add");
    const btn = document.createElement("input");
    btn.setAttribute("value", "ADD TO CARD");
    btn.setAttribute("type", "button");
    btn.classList.add("btn-add-product")
    divBtn.append(btn);
    u.innerHTML = card;
    u.append(productColour, productSize, divBtn);
    mas.push(u);
    u.addEventListener("click", (e) => {
        if (!e.target.classList.contains("btn-add-product")) {
            openInfo();
            descriptionCard(obj);
        }
        else if (e.target.classList.contains("btn-add-product")) {
            addProduct(obj);
            window.open(`./page3/index.html`);
        }
    })
}
function picRandom() {
    const pic = document.querySelector(".conteiner2-1");
    let c = mas.map(item => item);
    for (let i = 0; i < 4; i++) {
        let rand = Math.floor(Math.random() * mas.length);
        const item = document.createElement('div');
        item.classList.add("card2")
        pic.append(item);
        item.insertAdjacentElement("afterbegin", c[rand]);
        c.splice(rand, 1);
    }
}
picRandom();
function createOptions() {
    const datalist = document.querySelector("#inp-search");
    data.forEach((obj) => {
        const options = document.createElement("option");
        datalist.prepend(options);
        options.value = obj.title;
    })
};
createOptions();
const modal = document.querySelector(".container-modal");
const modalBody = document.querySelector(".modal-body");
const modalClose = document.getElementById("modal-close");
function openInfo() {
    modalBody.innerText = "";
    modal.classList.remove("hide");
    modalClose.addEventListener("click", () => {
        modal.classList.add("hide")
    });
}
function descriptionCard(obj) {
    const f = document.createElement("div");
    f.classList.add("description-card");
    const imgCard = `<div class="img-product"><img src="${obj.image}" alt=""></div>`;
    const descProduct = document.createElement("div");
    descProduct.classList.add("description-product");
    const infoCard = `
            <div class="name-product">${obj.title}</div>
            <div class="rating">
            <svg width="150" height="25" viewBox="0 0 150 25" fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <path
                d="M12.1795 0L15.7587 7.25312L23.7629 8.41582L17.9708 14.0612L19.3384 22.0329L12.1795 18.2688L5.02056 22.0329L6.38819 14.0612L0.596107 8.41582L8.60027 7.25312L12.1795 0Z"
                fill="#FFCC48" />
            <path
                d="M43.5896 0L47.1689 7.25312L55.173 8.41582L49.3809 14.0612L50.7486 22.0329L43.5896 18.2688L36.4307 22.0329L37.7983 14.0612L32.0063 8.41582L40.0104 7.25312L43.5896 0Z"
                fill="#FFCC48" />
            <path
                d="M75 0L78.5793 7.25312L86.5834 8.41582L80.7913 14.0612L82.159 22.0329L75 18.2688L67.8411 22.0329L69.2087 14.0612L63.4167 8.41582L71.4208 7.25312L75 0Z"
                fill="#FFCC48" />
            <path
                d="M106.41 0L109.989 7.25312L117.994 8.41582L112.201 14.0612L113.569 22.0329L106.41 18.2688L99.2513 22.0329L100.619 14.0612L94.8268 8.41582L102.831 7.25312L106.41 0Z"
                fill="#FFCC48" />
            <path
                d="M137.821 0L141.4 7.25312L149.404 8.41582L143.612 14.0612L144.98 22.0329L137.821 18.2688L130.662 22.0329L132.029 14.0612L126.237 8.41582L134.241 7.25312L137.821 0Z"
                fill="#FFCC48" />
        </svg> 
            </div>
            <div class="price-product">${obj.price} грн</div>
            <div class="text10">COLOR:</div>`;
    descProduct.innerHTML = infoCard;
    const productColour = document.createElement("div");
    productColour.classList.add("product-color");
    const colour = Object.values(obj.color);
    colour.forEach((el) => {
        if (el !== "") {
            const inpColour = document.createElement("input");
            inpColour.setAttribute("type", "button");
            inpColour.className = `${el}`;
            productColour.prepend(inpColour);
        }
        else {
            return;
        }
    });
    const textSize = document.createElement("div");
    textSize.classList.add("text10");
    textSize.innerText = "SIZE:";
    const productSize = document.createElement("div");
    productSize.classList.add("size1");
    const size = Object.values(obj.size);
    size.forEach((el) => {
        if (el !== "") {
            const inpSize = document.createElement("input");
            inpSize.setAttribute("type", "button");
            inpSize.value = `${el}`;
            productSize.prepend(inpSize);
        }
        else {
            return;
        }
    });
    const divBtn = document.createElement("div");
    divBtn.classList.add("btn2");
    const btn = document.createElement("input");
    btn.setAttribute("value", "ADD TO CARD");
    btn.setAttribute("type", "button");
    btn.classList.add("btn-add-1");
    const btn1 = document.createElement("input");
    btn1.setAttribute("value", "ADD TO WISHLIST");
    btn1.setAttribute("type", "button");
    btn1.classList.add("btn-add-2");
    divBtn.append(btn, btn1);
    const soc = document.createElement("div");
    soc.classList.add("soc");
    const facebook = document.createElement("input");
    facebook.classList.add("fb");
    const twitter = document.createElement("input");
    twitter.classList.add("tw");
    const pinterest = document.createElement("input");
    pinterest.classList.add("pint");
    const link = document.createElement("input");
    link.classList.add("link");
    soc.prepend(facebook, twitter, pinterest, link);
    const text = document.createElement("div");
    text.classList.add("text11");
    text.innerText = "- Worry Free Shopping -"
    const text2 = document.createElement("div");
    text2.classList.add("t-m-end");
    text2.innerHTML = `<div><span class = "img-end"><svg width="35" height="36" viewBox="0 0 35 36" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g clip-path="url(#clip0_168_532)">
        <path d="M33.0645 18.4464H30.1222V10.4391C30.1222 9.37365 29.2616 8.50688 28.2038 8.50688H23.7761V6.58717C23.7761 5.5542 22.9417 4.71387 21.9162 4.71387H1.85992C0.834395 4.71387 0 5.5542 0 6.58717V7.41318C0 7.69843 0.229551 7.92956 0.512695 7.92956C0.79584 7.92956 1.02539 7.69843 1.02539 7.41318V6.58717C1.02539 6.12373 1.39979 5.74663 1.85992 5.74663H21.9161C22.3762 5.74663 22.7506 6.12373 22.7506 6.58717V22.8507H1.02539V9.83433C1.02539 9.54908 0.79584 9.31795 0.512695 9.31795C0.229551 9.31795 0 9.54908 0 9.83433V27.3384C0 28.3713 0.834395 29.2117 1.85992 29.2117H3.03816C3.29595 30.6272 4.52983 31.7033 6.00865 31.7033C7.48747 31.7033 8.72136 30.6272 8.97914 29.2117H17.1349C17.418 29.2117 17.6476 28.9805 17.6476 28.6953C17.6476 28.41 17.418 28.1789 17.1349 28.1789H8.99056C8.76025 26.7303 7.51078 25.6199 6.00858 25.6199C4.50639 25.6199 3.25698 26.7303 3.02668 28.179H1.85992C1.39979 28.179 1.02539 27.8019 1.02539 27.3384V23.8835H22.7506V28.179H19.4865C19.2034 28.179 18.9738 28.4101 18.9738 28.6953C18.9738 28.9806 19.2034 29.2117 19.4865 29.2117H26.1356C26.3933 30.6273 27.6272 31.7034 29.106 31.7034C30.5847 31.7034 31.8187 30.6273 32.0765 29.2117H33.0815C34.1394 29.2117 34.9999 28.3449 34.9999 27.2795V20.3959C35 19.3209 34.1318 18.4464 33.0645 18.4464ZM6.00858 26.6527C7.10842 26.6527 8.00317 27.5539 8.00317 28.6617C8.00317 29.7694 7.10842 30.6707 6.00858 30.6707C4.90875 30.6707 4.01399 29.7694 4.01399 28.6617C4.01399 27.5539 4.90882 26.6527 6.00858 26.6527ZM29.1061 30.6706C28.0062 30.6706 27.1115 29.7694 27.1115 28.6616C27.1115 27.5539 28.0062 26.6527 29.1061 26.6527C30.2059 26.6527 31.1006 27.5539 31.1006 28.6616C31.1006 29.7694 30.2059 30.6706 29.1061 30.6706ZM33.9746 23.1957H32.6102C32.3276 23.1957 32.0975 22.964 32.0975 22.6793C32.0975 22.3945 32.3275 22.1629 32.6102 22.1629H33.9746V23.1957ZM33.9746 21.1301H32.6102C31.7622 21.1301 31.0721 21.8251 31.0721 22.6793C31.0721 23.5334 31.7621 24.2284 32.6102 24.2284H33.9746V27.2794C33.9746 27.7754 33.574 28.1789 33.0816 28.1789H32.088C31.8577 26.7303 30.6083 25.6199 29.1061 25.6199C27.6039 25.6199 26.3545 26.7303 26.1241 28.1789H23.776V9.53965H28.2037C28.6962 9.53965 29.0968 9.94311 29.0968 10.4391V10.5594H25.6642C25.3811 10.5594 25.1515 10.7905 25.1515 11.0758V18.9628C25.1515 19.2481 25.3811 19.4792 25.6642 19.4792H33.0645C33.5663 19.4792 33.9745 19.8905 33.9745 20.3958V21.1301H33.9746ZM29.0968 11.5922V18.4464H26.177V11.5922H29.0968Z" fill="#333333"/>
        <path d="M26.8494 21.1301H25.6641C25.3809 21.1301 25.1514 21.3613 25.1514 21.6465C25.1514 21.9318 25.3809 22.1629 25.6641 22.1629H26.8494C27.1326 22.1629 27.3621 21.9318 27.3621 21.6465C27.3621 21.3613 27.1326 21.1301 26.8494 21.1301Z" fill="#333333"/>
        <path d="M14.8953 13.1442C15.1784 13.1442 15.4079 12.913 15.4079 12.6278C15.4079 12.3425 15.1784 12.1114 14.8953 12.1114H12.8457C12.5626 12.1114 12.333 12.3425 12.333 12.6278V16.7577C12.333 17.0429 12.5626 17.2741 12.8457 17.2741H14.8953C15.1784 17.2741 15.4079 17.0429 15.4079 16.7577C15.4079 16.4724 15.1784 16.2413 14.8953 16.2413H13.3584V15.2091H14.8953C15.1784 15.2091 15.4079 14.978 15.4079 14.6928C15.4079 14.4075 15.1784 14.1764 14.8953 14.1764H13.3584V13.1442H14.8953Z" fill="#333333"/>
        <path d="M18.927 13.1442C19.2102 13.1442 19.4397 12.913 19.4397 12.6278C19.4397 12.3425 19.2102 12.1114 18.927 12.1114H16.8774C16.5943 12.1114 16.3647 12.3425 16.3647 12.6278V16.7577C16.3647 17.0429 16.5943 17.2741 16.8774 17.2741H18.927C19.2102 17.2741 19.4397 17.0429 19.4397 16.7577C19.4397 16.4724 19.2102 16.2413 18.927 16.2413H17.3901V15.2091H18.927C19.2102 15.2091 19.4397 14.978 19.4397 14.6928C19.4397 14.4075 19.2102 14.1764 18.927 14.1764H17.3901V13.1442H18.927Z" fill="#333333"/>
        <path d="M6.89818 13.1442C7.18133 13.1442 7.41088 12.913 7.41088 12.6278C7.41088 12.3425 7.18133 12.1114 6.89818 12.1114H4.84863C4.56549 12.1114 4.33594 12.3425 4.33594 12.6278V16.7577C4.33594 17.0429 4.56549 17.2741 4.84863 17.2741C5.13178 17.2741 5.36133 17.0429 5.36133 16.7577V15.2091H6.89818C7.18133 15.2091 7.41088 14.978 7.41088 14.6928C7.41088 14.4075 7.18133 14.1764 6.89818 14.1764H5.36133V13.1442H6.89818Z" fill="#333333"/>
        <path d="M11.3757 14.6928V12.6278C11.3757 12.3426 11.1462 12.1115 10.863 12.1115H8.81348C8.53033 12.1115 8.30078 12.3426 8.30078 12.6278V16.7577C8.30078 17.043 8.53033 17.2741 8.81348 17.2741C9.09662 17.2741 9.32617 17.043 9.32617 16.7577V15.3673L10.4365 17.0442C10.5352 17.1935 10.6978 17.2742 10.8635 17.2742C10.9613 17.2742 11.06 17.2461 11.1475 17.1874C11.383 17.0291 11.4467 16.7085 11.2896 16.4712L10.454 15.2092H10.863C11.1462 15.2092 11.3757 14.978 11.3757 14.6928ZM10.3503 14.1764H9.32617V13.1442H10.3503V14.1764Z" fill="#333333"/>
        </g>
        <defs>
        <clipPath id="clip0_168_532">
        <rect width="35" height="35.2518" fill="white" transform="translate(0 0.582733)"/>
        </clipPath>
        </defs>
        </svg></span> <span class = "text12">FREE PRIORITY SHIPPING 
             ON ORDERS $99+*</span></div><div><span><svg width="30" height="31" viewBox="0 0 30 31" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M29.6965 7.13272L18.0126 0.670781C17.837 0.573729 17.6241 0.573729 17.4486 0.670781L13.0447 3.10631C12.9517 3.13098 12.865 3.17801 12.7931 3.24555L5.76485 7.13272C5.5774 7.23623 5.4609 7.43448 5.4609 7.65003V15.4654C4.27529 15.8389 3.18879 16.4975 2.28539 17.4076C-0.0693486 19.779 -0.669706 23.396 0.791476 26.4079C0.933383 26.7007 1.28426 26.8221 1.57494 26.679C1.86585 26.5358 1.98624 26.1824 1.84433 25.8897C0.602878 23.3306 1.11306 20.2574 3.11394 18.2421C4.36524 16.9818 6.02875 16.2877 7.79824 16.2877C9.56773 16.2877 11.2312 16.9818 12.4825 18.2421C13.7336 19.5022 14.4228 21.1777 14.4228 22.9599C14.4228 24.7421 13.7336 26.4176 12.4823 27.6779C10.4814 29.6931 7.4302 30.207 4.88938 28.9566C4.5987 28.8134 4.24782 28.9349 4.10592 29.2277C3.96378 29.5207 4.08417 29.8741 4.37508 30.017C5.46433 30.5532 6.633 30.8137 7.7932 30.8137C9.81721 30.8135 11.8142 30.02 13.3111 28.5124C13.9975 27.821 14.54 27.0234 14.9238 26.1564L17.4486 27.5527C17.5365 27.6013 17.6335 27.6255 17.7306 27.6255C17.8276 27.6255 17.9249 27.6013 18.0126 27.5527L29.6965 21.091C29.884 20.9872 30.0005 20.789 30.0005 20.5737V16.4723C30.0005 16.1464 29.7382 15.8822 29.4145 15.8822C29.0909 15.8822 28.8286 16.1464 28.8286 16.4723V20.2244L18.3183 26.0372V14.4601L21.787 12.5416V15.5827C21.787 15.7916 21.8964 15.9848 22.0747 16.0908C22.1668 16.1455 22.2698 16.1729 22.373 16.1729C22.47 16.1729 22.5673 16.1487 22.6554 16.0998L25.1592 14.7118C25.3464 14.6081 25.4624 14.41 25.4624 14.195V10.5088L28.8286 8.64729V11.7514C28.8286 12.0773 29.0909 12.3415 29.4145 12.3415C29.7382 12.3415 30.0005 12.0773 30.0005 11.7514V7.65003C30.0005 7.43471 29.884 7.23623 29.6965 7.13272ZM17.7306 1.86123L28.1971 7.65003L24.8955 9.47604L14.4289 3.68724L17.7306 1.86123ZM17.7306 13.4386L7.26403 7.65003L10.6867 5.75693L21.1535 11.5455L17.7306 13.4386ZM22.3707 10.8724L11.9042 5.08379L13.2115 4.36062L23.6783 10.1492L22.3707 10.8724ZM13.3111 17.4076C11.8385 15.9244 9.88061 15.1076 7.79824 15.1076C7.40433 15.1076 7.01523 15.1369 6.63277 15.1943V8.64729L17.1467 14.4621V26.0393L15.3218 25.03C15.5015 24.3638 15.5944 23.669 15.5944 22.9599C15.5944 20.8625 14.7835 18.8906 13.3111 17.4076ZM24.2908 13.8462L22.9589 14.5845V11.8936L24.2908 11.1571V13.8462Z" fill="#333333"/>
        </svg>
        </span><span  class = "text12">FREE RETURNS & EXCHANGES*</span></div>`
    descProduct.append(productColour, textSize, productSize, divBtn, soc, text, text2);
    f.innerHTML = imgCard;
    f.append(descProduct);
    modalBody.prepend(f);
}
class Card {
    constructor(name, price) {
        this.name = name;
        this.price = price;
        this.quantity = "1";
    }
}
let cardsData = [];
function addProduct(obj) {
    const name = obj.title;
    const price = obj.price;

    if (localStorage.cardsData) {
        cardsData = JSON.parse(localStorage.getItem("cardsData"));
    }
    cardsData.push(new Card(name, price));
    localStorage.setItem("cardsData", JSON.stringify(cardsData));

}





