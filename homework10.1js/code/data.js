
const data = [
    {
        "id": 1,
        "image": "https://intertop.ua/load/Evo1359/412x550/2.jpg",
        "title": "Поло з довгим рукавом",
        "color":{
        "color1": "green",
        "color2": "blue",
        "color3": "white",
        "color4": "",
        "color5": "", 
        },
        
        "price": 1200,
        "size": {
            "size1": "40",
            "size2": "38",
            "size3": "36",
            "size4": "",
            "size5": "",

        }

    },
    {
        "id": 2,
        "image": "https://intertop.ua/load/231BR7418.T3000/412x550/MAIN.jpeg",
        "title": "Лонгслів",
        "color":{
            "color1": "black",
            "color2": "blue",
            "color3": "yellow",
            "color4": "",
            "color5": "", 
            },
        "price": 4590,
        "size": {
            "size1": "42",
            "size2": "40",
            "size3": "38",
            "size4": "36",
            "size5": "34",

        }

    },
    {
        "id": 3,
        "image": "https://intertop.ua/load/120140FLA-MX/412x550/MAIN.jpeg",
        "title": "Худі",
        "color":{
            "color1": "green",
            "color2": "blue",
            "color3": "white",
            "color4": "black",
            "color5": "", 
            },
        "price": 2599,
        "size": {
            "size1": "46",
            "size2": "44",
            "size3": "36",
            "size4": "34",
            "size5": "",

        }

    },
    {
        "id": 4,
        "image": "https://intertop.ua/load/TRL3234/412x550/MAIN.jpg",
        "title": "Шорти",
        "color":{
            "color1": "braun",
            "color2": "green",
            "color3": "white",
            "color4": "black",
            "color5": "", 
            },
        "price": 377,
        "size": {
            "size1": "42",
            "size2": "40",
            "size3": "38",
            "size4": "36",
            "size5": "",

        }

    },
    {
        "id": 5,
        "image": "https://intertop.ua/load/TRL3289/412x550/MAIN.jpg",
        "title": "Футболка",
        "color":{
            "color1": "green",
            "color2": "blue",
            "color3": "white",
            "color4": "black",
            "color5": "", 
            },
        "price": 215,
        "size": {
            "size1": "46",
            "size2": "44",
            "size3": "36",
            "size4": "34",
            "size5": "",

        }

    },
    {
        "id": 6,
        "image": "https://intertop.ua/load/TRL1096/412x550/98.jpg",
        "title": "Топи",
        "color":{
            "color1": "braun",
            "color2": "blue",
            "color3": "white",
            "color4": "black",
            "color5": "green", 
            },
        "price": 272,
        "size": {
            "size1": "40",
            "size2": "38",
            "size3": "36",
            "size4": "34",
            "size5": "32",

        }

    },
    {
        "id": 7,
        "image": "https://intertop.ua/load/TRL218/412x550/MAIN.jpg",
        "title": "Сукня максі",
        "color":{
            "color1": "",
            "color2": "blue",
            "color3": "",
            "color4": "",
            "color5": "", 
            },
        "price": 630,
        "size": {
            "size1": "46",
            "size2": "44",
            "size3": "",
            "size4": "",
            "size5": "",

        }

    },
    {
        "id": 8,
        "image": "https://intertop.ua/load/TAS131/412x550/MAIN.jpg",
        "title": "Футболка",
        "color":{
            "color1": "green",
            "color2": "blue",
            "color3": "white",
            "color4": "black",
            "color5": "yellow", 
            },
        "price": 599,
        "size": {
            "size1": "34",
            "size2": "32",
            "size3": "",
            "size4": "",
            "size5": "",

        }

    },
    {
        "id": 9,
        "image": "https://intertop.ua/load/TRL4211/412x550/MAIN.jpg",
        "title": "Шорти бермуди",
        "color":{
            "color1": "",
            "color2": "blue",
            "color3": "white",
            "color4": "black",
            "color5": "", 
            },
        "price": 1499,
        "size": {
            "size1": "46",
            "size2": "",
            "size3": "36",
            "size4": "",
            "size5": "30",

        }

    },
    {
        "id": 10,
        "image": "https://intertop.ua/load/TRL4295/412x550/MAIN.jpg",
        "title": "Сукня міні",
        "color":{
            "color1": "green",
            "color2": "blue",
            "color3": "white",
            "color4": "black",
            "color5": "yellow", 
            },
        "price": 1499,
        "size": {
            "size1": "46",
            "size2": "44",
            "size3": "",
            "size4": "34",
            "size5": "",

        }

    },

    {
        "id": 11,
        "image": "https://intertop.ua/load/TRL4351/412x550/MAIN.jpg",
        "title": "Футболка",
        "color":{
            "color1": "",
            "color2": "blue",
            "color3": "white",
            "color4": "black",
            "color5": "", 
            },
        "price": 499,
        "size": {
            "size1": "",
            "size2": "44",
            "size3": "",
            "size4": "34",
            "size5": "",

        }

    },
   
    {
        "id": 12,
        "image": "https://intertop.ua/load/PK1837/412x550/MAIN.jpg",
        "title": "Спідниця міні",
        "color":{
            "color1": "green",
            "color2": "blue",
            "color3": "",
            "color4": "",
            "color5": "", 
            },
        "price": 3999,
        "size": {
            "size1": "",
            "size2": "",
            "size3": "36",
            "size4": "34",
            "size5": "32",

        }

    },
    {
        "id": 13,
        "image": "https://intertop.ua/load/DEW2277/412x550/MAIN.jpg",
        "title": "Сукня міді",
        "color":{
            "color1": "green",
            "color2": "",
            "color3": "white",
            "color4": "",
            "color5": "", 
            },
        "price": 2599,
        "size": {
            "size1": "46",
            "size2": "44",
            "size3": "",
            "size4": "",
            "size5": "",

        }

    },
    
    {
        "id": 14,
        "image": "https://intertop.ua/load/SO102/412x550/MAIN.jpg",
        "title": "Футболка",
        "color":{
            "color1": "",
            "color2": "",
            "color3": "white",
            "color4": "black",
            "color5": "", 
            },
        "price": 599,
        "size": {
            "size1": "46",
            "size2": "",
            "size3": "36",
            "size4": "34",
            "size5": "32",

        }

    },
    {
        "id": 15,
        "image": "https://intertop.ua/load/SO98/412x550/MAIN.jpg",
        "title": "Футболка",
        "color":{
            "color1": "",
            "color2": "",
            "color3": "",
            "color4": "black",
            "color5": "", 
            },
        "price": 599,
        "size": {
            "size1": "46",
            "size2": "44",
            "size3": "",
            "size4": "",
            "size5": "",

        }

    },
    {
        "id": 16,
        "image": "https://intertop.ua/load/SO103/412x550/MAIN.jpg",
        "title": "Футболка",
        "color":{
            "color1": "green",
            "color2": "blue",
            "color3": "",
            "color4": "",
            "color5": "", 
            },
        "price": 599,
        "size": {
            "size1": "",
            "size2": "44",
            "size3": "",
            "size4": "34",
            "size5": "32",

        }

    },
    {
        "id": 17,
        "image": "https://intertop.ua/load/SO100/412x550/MAIN.jpg",
        "title": "Футболка",
        "color":{
            "color1": "green",
            "color2": "blue",
            "color3": "white",
            "color4": "black",
            "color5": "", 
            },
        "price": 599,
        "size": {
            "size1": "",
            "size2": "",
            "size3": "",
            "size4": "",
            "size5": "32",

        }

    },
    {
        "id": 18,
        "image": "https://intertop.ua/load/SO101/412x550/MAIN.jpg",
        "title": "Футболка",
        "color":{
            "color1": "",
            "color2": "",
            "color3": "",
            "color4": "black",
            "color5": "", 
            },
        "price": 599,
        "size": {
            "size1": "46",
            "size2": "44",
            "size3": "36",
            "size4": "34",
            "size5": "",

        }

    },
    {
        "id": 19,
        "image": "https://intertop.ua/load/KY416/412x550/MAIN.jpg",
        "title": "Велосипедки",
        "color":{
            "color1": "",
            "color2": "blue",
            "color3": "white",
            "color4": "black",
            "color5": "", 
            },
        "price": 1599,
        "size": {
            "size1": "46",
            "size2": "44",
            "size3": "36",
            "size4": "34",
            "size5": "30",

        }

    },
    {
        "id": 20,
        "image": "https://intertop.ua/load/KY420/412x550/MAIN.jpg",
        "title": "Фктболка спортивна",
        "color":{
            "color1": "",
            "color2": "blue",
            "color3": "white",
            "color4": "black",
            "color5": "yellow", 
            },
        "price": 1399,
        "size": {
            "size1": "",
            "size2": "40",
            "size3": "36",
            "size4": "34",
            "size5": "",

        }

    },
    {
        "id": 21,
        "image": "https://intertop.ua/load/KY406/412x550/MAIN.jpg",
        "title": "Легінці",
        "color":{
            "color1": "grey",
            "color2": "blue",
            "color3": "white",
            "color4": "black",
            "color5": "braun", 
            },
        "price": 1299,
        "size": {
            "size1": "46",
            "size2": "42",
            "size3": "36",
            "size4": "34",
            "size5": "32",

        }

    },
    {
        "id": 22,
        "image": "https://intertop.ua/load/MQ254/412x550/MAIN.JPG",
        "title": "Футболка",
        "color":{
            "color1": "",
            "color2": "",
            "color3": "",
            "color4": "black",
            "color5": "", 
            },
        "price": 676,
        "size": {
            "size1": "",
            "size2": "44",
            "size3": "",
            "size4": "34",
            "size5": "30",

        }

    },
    {
        "id": 23,
        "image": "https://intertop.ua/load/PK1689/412x550/MAIN.jpg",
        "title": "Шорти чінос",
        "color":{
            "color1": "",
            "color2": "blue",
            "color3": "",
            "color4": "",
            "color5": "", 
            },
        "price": 1679,
        "size": {
            "size1": "",
            "size2": "",
            "size3": "36",
            "size4": "34",
            "size5": "30",

        }

    },
    {
        "id": 24,
        "image": "https://intertop.ua/load/KTW18/412x550/MAIN.jpg",
        "title": "Сукня міді",
        "color":{
            "color1": "",
            "color2": "blue",
            "color3": "",
            "color4": "",
            "color5": "", 
            },
        "price": 339,
        "size": {
            "size1": "46",
            "size2": "44",
            "size3": "42",
            "size4": "40",
            "size5": "",

        }

    },
    {
        "id": 25,
        "image": "https://intertop.ua/load/PF5495/412x550/MAIN.jpg",
        "title": "Футболка",
        "color":{
            "color1": "grey",
            "color2": "blue",
            "color3": "white",
            "color4": "black",
            "color5": "", 
            },
        "price": 727,
        "size": {
            "size1": "",
            "size2": "44",
            "size3": "36",
            "size4": "34",
            "size5": "",

        }

    },
    {
        "id": 26,
        "image": "https://intertop.ua/load/KTW736/412x550/MAIN.jpg",
        "title": "Блуза з коротким рукавом",
        "color":{
            "color1": "grey",
            "color2": "blue",
            "color3": "",
            "color4": "black",
            "color5": "", 
            },
        "price": 279,
        "size": {
            "size1": "46",
            "size2": "44",
            "size3": "36",
            "size4": "34",
            "size5": "",

        }

    },
    {
        "id": 27,
        "image": "https://intertop.ua/load/OVF132/412x550/MAIN.jpg",
        "title": "Футболка",
        "color":{
            "color1": "",
            "color2": "blue",
            "color3": "white",
            "color4": "",
            "color5": "yellow", 
            },
        "price": 375,
        "size": {
            "size1": "46",
            "size2": "44",
            "size3": "",
            "size4": "34",
            "size5": "",

        }

    },
    {
        "id": 28,
        "image": "https://intertop.ua/load/TRL4326/412x550/MAIN.jpg",
        "title": "Шорти",
        "color":{
            "color1": "",
            "color2": "blue",
            "color3": "",
            "color4": "",
            "color5": "", 
            },
        "price": 1399,
        "size": {
            "size1": "40",
            "size2": "40",
            "size3": "36",
            "size4": "34",
            "size5": "32",

        }

    },
    {
        "id": 29,
        "image": "https://intertop.ua/load/PF6446/412x550/MAIN.jpg",
        "title": "Сукня міні",
        "color":{
            "color1": "green",
            "color2": "blue",
            "color3": "",
            "color4": "",
            "color5": "", 
            },
        "price": 3999,
        "size": {
            "size1": "46",
            "size2": "44",
            "size3": "36",
            "size4": "34",
            "size5": "",

        }

    },
    {
        "id": 30,
        "image": "https://intertop.ua/load/TRL3007/412x550/MAIN.jpg",
        "title": "Майка",
        "color":{
            "color1": "",
            "color2": "",
            "color3": "white",
            "color4": "black",
            "color5": "", 
            },
        "price": 287,
        "size": {
            "size1": "",
            "size2": "",
            "size3": "36",
            "size4": "34",
            "size5": "",

        }

    },
    {
        "id": 31,
        "image": "https://intertop.ua/load/TRL4111/412x550/MAIN.jpg",
        "title": "Спідниця максі",
        "color":{
            "color1": "",
            "color2": "blue",
            "color3": "white",
            "color4": "black",
            "color5": "", 
            },
        "price": 1599,
        "size": {
            "size1": "46",
            "size2": "44",
            "size3": "42",
            "size4": "40",
            "size5": "30",

        }

    },
    {
        "id": 32,
        "image": "https://intertop.ua/load/TRL4016/412x550/95.jpg",
        "title": "Прямі джинси",
        "color":{
            "color1": "grey",
            "color2": "blue",
            "color3": "",
            "color4": "",
            "color5": "", 
            },
        "price": 604,
        "size": {
            "size1": "40",
            "size2": "38",
            "size3": "36",
            "size4": "34",
            "size5": "",

        }

    },
    {
        "id": 33,
        "image": "https://intertop.ua/load/INK26/412x550/MAIN.jpg",
        "title": "Сукня міді",
        "color":{
            "color1": "grey",
            "color2": "",
            "color3": "white",
            "color4": "black",
            "color5": "", 
            },
        "price": 799,
        "size": {
            "size1": "",
            "size2": "38",
            "size3": "36",
            "size4": "34",
            "size5": "32",

        }

    },
    {
        "id": 34,
        "image": "https://intertop.ua/load/TRL2849/412x550/1.jpg",
        "title": "Сукня міні",
        "color":{
            "color1": "green",
            "color2": "blue",
            "color3": "white",
            "color4": "black",
            "color5": "", 
            },
        "price": 599,
        "size": {
            "size1": "46",
            "size2": "44",
            "size3": "36",
            "size4": "34",
            "size5": "30",

        }

    },
    {
        "id": 35,
        "image": "https://intertop.ua/load/TRL3381/412x550/MAIN.jpg",
        "title": "Сукня міні",
        "color":{
            "color1": "",
            "color2": "",
            "color3": "white",
            "color4": "",
            "color5": "", 
            },
        "price": 458,
        "size": {
            "size1": "",
            "size2": "44",
            "size3": "36",
            "size4": "34",
            "size5": "32",

        }

    },
    {
        "id": 36,
        "image": "https://intertop.ua/load/TRL3371/412x550/1.jpg",
        "title": "Сукня міні",
        "color":{
            "color1": "",
            "color2": "blue",
            "color3": "white",
            "color4": "black",
            "color5": "yellow", 
            },
        "price": 431,
        "size": {
            "size1": "42",
            "size2": "38",
            "size3": "36",
            "size4": "34",
            "size5": "",

        }

    },
    {
        "id": 37,
        "image": "https://intertop.ua/load/TRL3370/412x550/MAIN.jpg",
        "title": "Сукня міні",
        "color":{
            "color1": "broun",
            "color2": "",
            "color3": "white",
            "color4": "black",
            "color5": "", 
            },
        "price": 503,
        "size": {
            "size1": "",
            "size2": "44",
            "size3": "",
            "size4": "",
            "size5": "",

        }

    },
    {
        "id": 38,
        "image": "https://intertop.ua/load/TRL3366/412x550/1.jpg",
        "title": "Сукня міні",
        "color":{
            "color1": "green",
            "color2": "blue",
            "color3": "",
            "color4": "",
            "color5": "", 
            },
        "price": 503,
        "size": {
            "size1": "",
            "size2": "38",
            "size3": "36",
            "size4": "32",
            "size5": "",

        }

    },
    {
        "id": 39,
        "image": "https://intertop.ua/load/TRL2999/412x550/MAIN.jpg",
        "title": "Сукня міні",
        "color":{
            "color1": "",
            "color2": "",
            "color3": "",
            "color4": "black",
            "color5": "", 
            },
        "price": 836,
        "size": {
            "size1": "46",
            "size2": "44",
            "size3": "",
            "size4": "",
            "size5": "",

        }

    },
    {
        "id": 40,
        "image": "https://intertop.ua/load/TRL2871/412x550/MAIN.jpg",
        "title": "Сукня-футболка",
        "color":{
            "color1": "green",
            "color2": "",
            "color3": "",
            "color4": "",
            "color5": "", 
            },
        "price": 557,
        "size": {
            "size1": "",
            "size2": "40",
            "size3": "36",
            "size4": "34",
            "size5": "",

        }

    },
    {
        "id": 41,
        "image": "https://intertop.ua/load/TRL2869/412x550/MAIN.jpg",
        "title": "Сукня міні",
        "color":{
            "color1": "grey",
            "color2": "",
            "color3": "white",
            "color4": "",
            "color5": "", 
            },
        "price": 377,
        "size": {
            "size1": "",
            "size2": "",
            "size3": "36",
            "size4": "34",
            "size5": "32",

        }

    },
    {
        "id": 42,
        "image": "https://intertop.ua/load/TRL2818/412x550/97.jpg",
        "title": "Сарафан",
        "color":{
            "color1": "",
            "color2": "",
            "color3": "",
            "color4": "black",
            "color5": "", 
            },
        "price": 422,
        "size": {
            "size1": "40",
            "size2": "38",
            "size3": "",
            "size4": "34",
            "size5": "30",

        }

    },
    {
        "id": 43,
        "image": "https://intertop.ua/load/TRL231/412x550/1.jpg",
        "title": "Сукня максі",
        "color":{
            "color1": "grey",
            "color2": "",
            "color3": "white",
            "color4": "",
            "color5": "yellow", 
            },
        "price": 1043,
        "size": {
            "size1": "",
            "size2": "",
            "size3": "36",
            "size4": "34",
            "size5": "",

        }

    },
    {
        "id": 44,
        "image": "https://intertop.ua/load/TRL226/412x550/2.jpg",
        "title": "Сукня максі",
        "color":{
            "color1": "green",
            "color2": "blue",
            "color3": "",
            "color4": "",
            "color5": "yellow", 
            },
        "price": 765,
        "size": {
            "size1": "42",
            "size2": "40",
            "size3": "38",
            "size4": "34",
            "size5": "30",

        }

    },
    {
        "id": 45,
        "image": "https://intertop.ua/load/PF6334/412x550/MAIN.jpg",
        "title": "Поло",
        "color":{
            "color1": "",
            "color2": "",
            "color3": "white",
            "color4": "",
            "color5": "", 
            },
        "price": 2799,
        "size": {
            "size1": "46",
            "size2": "44",
            "size3": "",
            "size4": "",
            "size5": "",

        }

    },
    {
        "id": 46,
        "image": "https://intertop.ua/load/PF6333/412x550/2.jpg",
        "title": "Поло",
        "color":{
            "color1": "green",
            "color2": "blue",
            "color3": "",
            "color4": "",
            "color5": "", 
            },
        "price": 2799,
        "size": {
            "size1": "",
            "size2": "38",
            "size3": "32",
            "size4": "30",
            "size5": "",

        }

    },
    {
        "id": 47,
        "image": "https://intertop.ua/load/PF6215/412x550/4.jpg",
        "title": "Поло",
        "color":{
            "color1": "green",
            "color2": "blue",
            "color3": "white",
            "color4": "black",
            "color5": "", 
            },
        "price": 2199,
        "size": {
            "size1": "46",
            "size2": "44",
            "size3": "",
            "size4": "",
            "size5": "",

        }

    },
    {
        "id": 48,
        "image": "https://intertop.ua/load/RGT227/412x550/MAIN.jpg",
        "title": "Поло",
        "color":{
            "color1": "",
            "color2": "",
            "color3": "",
            "color4": "black",
            "color5": "", 
            },
        "price": 499,
        "size": {
            "size1": "42",
            "size2": "38",
            "size3": "",
            "size4": "32",
            "size5": "",

        }

    },
    {
        "id": 49,
        "image": "https://intertop.ua/load/3WF01041/412x550/MAIN.jpeg",
        "title": "Поло",
        "color":{
            "color1": "",
            "color2": "",
            "color3": "",
            "color4": "",
            "color5": "yellow", 
            },
        "price": 3495,
        "size": {
            "size1": "",
            "size2": "",
            "size3": "36",
            "size4": "34",
            "size5": "",

        }

    },
    {
        "id": 50,
        "image": "https://intertop.ua/load/T1685193943/412x550/MAIN.jpeg",
        "title": "Поло",
        "color":{
            "color1": "",
            "color2": "blue",
            "color3": "white",
            "color4": "",
            "color5": "", 
            },
        "price": 470,
        "size": {
            "size1": "40",
            "size2": "",
            "size3": "",
            "size4": "32",
            "size5": "30",

        }

    }
]

