const finish = document.querySelector(".two-window"),
    start = document.querySelector(".one-window");
let counter = 1;

function keyPressed(e) {
    const activ = e.target;
    if ("Enter" === e.key) {
        e.preventDefault();
        createLine(start, activ);
        "" !== activ.textContent && readSymbols(activ.textContent);
    }
    else if ("Backspace" === e.key && "" === activ.textContent) {
        activ.parentElement.remove();
    }
}

function readNode(e, lev) {
    if (e) {
        const regOne = /^[a-z].*?(?=\.|#|$)/,
            regTwo = /[.].*?(?=$)/,
            regThree = /[#].*?(?=$)/,
            oll = {};
        return e.match(regOne) && (oll.name = e.match(regOne).toString()),
            e.match(regTwo) && (oll.class = e.match(regTwo).toString().replaceAll(".", " ").trim()),
            e.match(regThree) && (oll.id = e.match(regThree).toString().slice(1)),
            oll.level = ++lev,
            oll
    }
}

class Part {
    constructor(e = {}) {
        this.element = document.createElement(e.name || "div"),
            this.id = e.id,
            this.class = e.class,
            this.children = e.children || [],
            this.element.level = e.level || 0,
            this.collect()
    }
    collect() {
        this.id && (this.element.id = this.id),
            this.class && (this.element.className = this.class),
            this.children.forEach((e => {
                const part = new Part(e);
                this.element.append(part.element)
            }
            ))
    }
    show() {
        const a = "",
            reg = /<[^>]+>/g;
        let b = "";
        return function contein(data) {
            const [...oll] = data.children;
            oll.forEach((e => {
                const first = e.outerHTML.match(reg)[0],
                    end = e.outerHTML.match(reg).at(-1);
        
                0 === e.children.length ? b += `${a.repeat(e.level - 1)}${first}${end}\br` : e.children.length > 0 ? (b += `${a.repeat(e.level - 1)}${first}\br`,
                    contein(e),
                    b += `${a.repeat(e.level - 1)}${end}\br`) : e.children.length > 0 ? (b += `${a.repeat(e.level - 1)}${first}\br`,
                        b += `${a.repeat(e.level)}\br`,
                        contein(e),
                        b += `${a.repeat(e.level - 1)}${end}\br`) : 0 === e.children.length ? (a += `${e.repeat(l.level - 1)}${first}\br`,
                            b += `${a.repeat(e.level)}$\br`,
                            b += `${a.repeat(e.level - 1)}${end}\br`) : 0 !== e.children.length || (b += `${a.repeat(e.level - 1)}${first}${end}\br`)
            }
            ))
        }(this.element),
            b
    }
}

function divideLine(e) {
    const divide = e.split("\br");
    divide.forEach((e => {
        createLine(finish, "", e)
    }
    ))
}

function readSymbols(text) {
    if (text) {
        const reg = /\^/g,
            obj = {};
        let info = obj;
        info.level = 0;
        text.split(">").forEach((e => {
            if (info.children = [],
                e.includes(" ")) {
                const node = readNode(e, info.level);
                info.children.push(node);
            }
            else if (e.includes("^")) {
                if (e.includes("^")) {
                    const ollSymbols = reg.exec(e).index,
                        firstSymbol = e.match(reg)[0].length,
                        firstPart = e.substring(0, ollSymbols),
                        lastPart = e.substring(ollSymbols + firstSymbol),
                        firstNode = readNode(firstPart, info.level);
                    info.children.push(firstNode);
                    const lastNode = readNode(lastPart, info.level);
                    info.children.push(lastNode),
                        info = lastNode;
                }
            } else {
                const node = readNode(e, info.level);
                info.children.push(node),
                    info = node
            }
        }
        )),
            divideLine(new Part(obj).show());
    }
}

function createLine(e, i, n = "") {
    const oll = document.createElement("div");
    const number = document.createElement("span");
    number.className = "number";
    e === start ? (i || (number.innerText = 1),
        i && (number.innerText = ++i.previousElementSibling.textContent)) : e === finish && (number.innerText = counter++);
    const text = document.createElement("span");
    e === start && text.setAttribute("contenteditable", !0);
    e === finish && (text.textContent = n);
    text.addEventListener("keydown", (e => {
        keyPressed(e)
    }
    ));
    oll.append(number, text);
    i ? i && (i.parentElement.after(oll),
        count(".one-window .number")) : e.append(oll);
    text.focus();
}

function count(e) {
    document.querySelectorAll(e).forEach(((e, i) => {
        e.innerText = ++i;
    }
    ))
}
createLine(start);