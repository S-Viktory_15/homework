const pizza = {
    userName: '',
    phone: '',
    email: '',
    size: 85,
    toppings: [],
    sauce: [],
    
}
const sum = [];
const pizzaToppingsAll = [];
const pizzaSaucesAll = [];

const price = {
    size: {
        small: 50,
        mid: 80,
        big: 85
    },
    toping: [
        { price: 20, name: "moc1", name1: "Сир звичайний" },
        { price: 45, name: "moc2", name1: "Сир фета" },
        { price: 12, name: "moc3", name1: "Моцарелла" },
        { price: 93, name: "telya", name1: "Телятина" },
        { price: 78, name: "vetch1", name1: "Помiдори" },
        { price: 34, name: "vetch2", name1: "Гриби" },
    ],
    sauce: [
        { price: 60, name: "sauceClassic", name1: "Кетчуп" },
        { price: 70, name: "sauceBBQ", name1: "BBQ" },
        { price: 50, name: "sauceRikotta", name1: "Рiкотта" },
    ]
}

window.addEventListener("DOMContentLoaded", () => {
    document.querySelector("form#pizza")
        .addEventListener("click", (e) => {
            //console.log(e.target.id);
            switch (e.target.id) {
                case "small": pizza.size = price.size.small;
                    break;
                case "mid": pizza.size = price.size.mid;
                    break;
                case "big": pizza.size = price.size.big;
                    break;
            }
            show(pizza)
            
            
        })
    show(pizza)
    document.querySelector("#banner")
        .addEventListener("mousemove", (e) => {
            randomPositionBanner(e.target, e.clientX, e.clientY)
        })

    document.querySelector("#banner button")
        .addEventListener("click", () => {
            alert("Ваш промокод : XXXXXX")
        })
})


function randomPositionBanner(banner) {
    const coords = {
        X: Math.floor(Math.random() * document.body.clientWidth),
        Y: Math.floor(Math.random() * document.body.clientHeight)
    }

    const width = (parseInt(getComputedStyle(document.querySelector("#banner"))["width"]) + 100)

    if (coords.X + width > document.body.clientWidth) {
        return
    }

    if (coords.Y + 100 > document.body.clientWidth) {
        return
    }
    console.log(coords)
    //banner.style.transform = `translateX(300px)`;
    //console.log();
    //document.body.clientWidth / clientHeight

    banner.style.left = coords.X + "px"
    banner.style.top = coords.Y + "px"
}


//валідація 
window.addEventListener('DOMContentLoaded', () => {
    const userName = document.querySelector("[name='name']");
    const userPhone = document.querySelector("[name='phone']");
    const userEmail = document.querySelector("[name='email']");
    const validate = (value, pattern) => pattern.test(value);

    userName.addEventListener('input', () => {
        if (validate(userName.value, /^[а-яіїєґ]{2,}$/i)) {
            userName.classList.add('success');
            userName.classList.remove('error');
            pizza.userName = userName.value;
        }
        else {
            userName.classList.remove('success');
            userName.classList.add('error');
        }
    })


    userPhone.addEventListener('input', () => {
        if (validate(userPhone.value, /^\+380[0-9]{9}$/)) {
            userPhone.classList.add('success');
            userPhone.classList.remove('error');
            pizza.phone = userPhone.value;
        }
        else {
            userPhone.classList.remove('success');
            userPhone.classList.add('error');
        }
    })
    userEmail.addEventListener('change', () => {
        if (validate(userEmail.value, /^[a-z0-9._]{3,40}@[a-z0-9-]{1,777}\.[.a-z]{2,10}$/i)) {
            userEmail.classList.add('success');
            userEmail.classList.remove('error');
            pizza.email = userEmail.value;
        }
        else {
            userEmail.classList.remove('success');
            userEmail.classList.add('error');
        }
    })

});

//Відображення складу
function show(pizza) {
    const h = document.querySelector("#price");
    // topping

     h.innerHTML = pizza.size + show1();
    

    
   
    


}

//Перетягування.
window.addEventListener("DOMContentLoaded", () => {
    const ingridients = document.querySelector(".ingridients"),
        table = document.querySelector(".table");

    ingridients.addEventListener("dragstart", (e) => {
        //e.target.classList.add("transfer")
        e.dataTransfer.setData("text", e.target.id);
    });

    table.addEventListener("dragenter", () => {
        table.classList.add("transfer")
    })

    table.addEventListener("dragleave", () => {
        table.classList.remove("transfer")
    })

    table.addEventListener("dragover", (e) => {
        e.preventDefault();
        e.stopPropagation();
    })

    table.addEventListener("drop", (e) => {
        e.preventDefault();

        const id = e.dataTransfer.getData("text")


        const img = document.createElement("img");
        img.src = document.getElementById(id).src;
        table.append(img)
        table.classList.remove("transfer")

 
if (id.includes("sauce")) {
    getSauce(id);
} else {
    getTopping(id);
}
        const topping = document.querySelector("#topping");
        topping.innerHTML = pizza.toppings.join("</br>");
        
       const sauce = document.querySelector("#sauce");
        sauce.innerHTML = pizza.sauce.join("</br>");
        
        if (id.includes("sauce")) {
            showSauce(id);
        } else {
           showToppings(id); 
        }
        show(pizza);

    })

});
function getTopping(id) {

    const nameToppping = price.toping.map(el => {
        if (id === el.name) {
            return el.name1;

        }
    }).filter((el) => el !== undefined).join("");

    pizzaToppingsAll.push(nameToppping);

    const numberToppings = pizzaToppingsAll.filter((item) => item === nameToppping).length;

    pizzaToppingsAll.forEach(el => {
        if (pizza.toppings.indexOf(el) === -1) {

            pizza.toppings.push(el);
        }
    });

    pizza.toppings[pizza.toppings.indexOf(nameToppping)] = pizza.toppings[pizza.toppings.indexOf(nameToppping)] + ` - ${numberToppings}`;

    if (pizza.toppings.some((e) => e === nameToppping + ` - ${numberToppings - 1}`)) {
        pizza.toppings.splice(pizza.toppings.indexOf(nameToppping + ` - ${numberToppings - 1}`), 1);

    };

    if (pizza.toppings.some((e) => e === "Сир звичайний")) {
        pizza.toppings.splice(pizza.toppings.indexOf("Сир звичайний"), 1)
    }
    if (pizza.toppings.some((e) => e === "Сир фета")) {
        pizza.toppings.splice(pizza.toppings.indexOf("Сир фета"), 1)
    }
    if (pizza.toppings.some((e) => e === "Моцарелла")) {
        pizza.toppings.splice(pizza.toppings.indexOf("Моцарелла"), 1)
    }
    if (pizza.toppings.some((e) => e === "Телятина")) {
        pizza.toppings.splice(pizza.toppings.indexOf("Телятина"), 1)
    }
    if (pizza.toppings.some((e) => e === "Помiдори")) {
        pizza.toppings.splice(pizza.toppings.indexOf("Помiдори"), 1)
    }
    if (pizza.toppings.some((e) => e === "Гриби")) {
        pizza.toppings.splice(pizza.toppings.indexOf("Гриби"), 1)
    }
}

function getSauce(id) {
    const nameSauce = price.sauce.map(el => {
        if (id === el.name) {
            return el.name1;
        }
    }).filter((el) => el !== undefined).join("");

    pizzaSaucesAll.push(nameSauce);

    const numberSauce = pizzaSaucesAll.filter((item) => item === nameSauce).length;

    pizzaSaucesAll.forEach(el => {
        if (pizza.sauce.indexOf(el) === -1) {

            pizza.sauce.push(el);
        }
    });

    pizza.sauce[pizza.sauce.indexOf(nameSauce)] = pizza.sauce[pizza.sauce.indexOf(nameSauce)] + ` - ${numberSauce}`;

    if (pizza.sauce.some((e) => e === nameSauce + ` - ${numberSauce - 1}`)) {
        pizza.sauce.splice(pizza.sauce.indexOf(nameSauce + ` - ${numberSauce - 1}`), 1);

    };

   if (pizza.sauce.some((e) => e === "Кетчуп")) {
        pizza.sauce.splice(pizza.sauce.indexOf("Кетчуп"), 1)
    }
    if (pizza.sauce.some((e) => e === "BBQ")) {
        pizza.sauce.splice(pizza.sauce.indexOf("BBQ"), 1)
    }
    if (pizza.sauce.some((e) => e === "Рiкотта")) {
        pizza.sauce.splice(pizza.sauce.indexOf("Рiкотта"), 1)
    }

};


function showSauce(id) {
    const saucePrice = +price.sauce.map(el => {
        if (id === el.name) {
           return  el.price;

        }
    }).filter((el) => el !== undefined).join("");
    sum.push(saucePrice);

};


function showToppings(id) {
    const toppingPrice = +price.toping.map(el => {
        if (id === el.name) {
           // return sum += el.price;
           return el.price;
        }
    }).filter((el) => el !== undefined).join("");
    sum.push(toppingPrice);
    
};

function show1() {

    return sum.reduce(function(a, b){
        return a+ b;
    }, 0)
    
    }











