function req(url, callback) {
    const xhr = new XMLHttpRequest();

    xhr.open("GET", url);
    xhr.send();
    xhr.addEventListener("readystatechange", () => {
        if(xhr.readyState === 4 && xhr.status >= 200 && xhr.status < 300){
         callback(JSON.parse(xhr.response));
        }else if (xhr.readyState === 4){
            alert("перезавантажте сторінку")
        }
    })
}

export default req;