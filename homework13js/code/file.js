

const category = document.location.search.substr(1);
document.querySelector("title").innerHTML = category;

async function req(url) {
    const data = await fetch(url)
    return data.json()
}

if (category === "characters") {
    req("https://rickandmortyapi.com/api/character")
        .then((data) => {
            showInfo(data)
        }).catch((e) => { console.error(e) })
} else if (category === "locations") {
    req("https://rickandmortyapi.com/api/location").then((data) => {
        showInfo(data)
    }).catch((e) => { console.error(e) });
} else if (category === "episodes") {
    req("https://rickandmortyapi.com/api/episode").then((data) => {
        showInfo(data)
    }).catch((e) => { console.error(e) });
} else {
    console.error("")
}


function showInfo(e) {
    const divs = e.results.map((el) => {
        const div = document.createElement("div");
        div.className = "card"
        const pattern = `<div class="img-card">
        ${el.image ? `<img src="${el.image}" alt="${el.name}">` : el.type ? el.type : el.air_date
            }
        </div>
        <h3>${el.name}</h3>
        ${el.species ? `<p>${el.species}</p>` : el.episode ? `<p>${el.episode}</p>` : ""}
        `
        div.insertAdjacentHTML("beforeend", pattern);
        div.addEventListener("click", (e) => {
            openInfo(el, e);
            getInput(el);
        })
        return div;
    });

    document.querySelector(".cards").append(...divs)
}

function openInfo(card, event) {
    const modal = document.querySelector(".box-modal");
    modal.classList.add("active");

    document.querySelector("#modal-closed").addEventListener("click", () => {

        modal.classList.remove("active");
    })
    console.log(Object.entries(card));
    document.querySelector(".modal-body").innerText = "";

}


function getInput(obj) {
    const modalBody = document.querySelector(".modal-body"),
        div1 = document.createElement("div"),
        div2 = document.createElement("div"),
        input = document.createElement("input");

    div1.className = "text-1-modal";
    div2.className = "text-2-modal";

    div1.innerText = `${obj.name}`;

    obj.gender ? div2.innerText = `${obj.gender}` : obj.air_date ? div2.innerText = `${obj.air_date}` : "";

    input.setAttribute("type", "button");
    input.className = "input"


    obj.origin ? input.setAttribute("value", `${obj.origin.name}`) : input.setAttribute("value", `${obj.name}`);

    modalBody.append(div1, div2, input);

    input.addEventListener("click", () => {
        const wind = window.open(`${obj.url}`);
    });

}

req("https://rickandmortyapi.com/api")
    .then((info) => {

        const arr = Object.entries(info);
        if (!Array.isArray(arr)) return;
        arr.forEach((item) => {
            getNavigation(item);

        })
    })

    function getNavigation(element) {

        const [key] = element;
        const nav = document.querySelectorAll("li");
        nav.forEach((e) => {
            e.addEventListener("click", () => {
                if (e.classList.value === key) {
                    const wind = window.open(`/page/index.html?${key}`)
                }
                else {
                    return;
                }
    
            })
    
        })
    }




