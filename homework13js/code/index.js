// https://rickandmortyapi.com/api 

async function getData(url, method = "GET") {
    const data = await fetch(url, { method });
    return data.json();
}


getData("https://rickandmortyapi.com/api")
    .then((info) => {

        const arr = Object.entries(info);
        if (!Array.isArray(arr)) return;
        arr.forEach((item) => {
            createCard(item);
            getNavigation(item);

        })
    })

function createCard(element) {

    const [key] = element
    const card = document.createElement("div");
    card.innerText = key;
    card.className = "card";
    card.addEventListener("click", () => {
        const wind = window.open(`/page/index.html?${key}`);
    })
    document.querySelector(".cards").append(card);
}


function getNavigation(element) {

    const [key] = element;
    const nav = document.querySelectorAll("li");
    nav.forEach((e) => {
        e.addEventListener("click", () => {
            if (e.classList.value === key) {
                const wind = window.open(`/page/index.html?${key}`)
            }
            else {
                return;
            }

        })

    })
}


if(matchMedia){
    const screen = window.matchMedia("(max-width: 768px)");
    screen.addEventListener("change", ()=>{
        changes(screen);
    })
}

function changes(screen){
    const card = document.querySelector(".cards");
    if(screen.matches){
       card.classList.add("cardsColum");
      
    }
    else{
        card.classList.remove("cardsColum");
        
    }
}


