//Дані два масиви: ['a', 'b', 'c'] та [1, 2, 3]. Об'єднайте їх разом.
document.write('<p>Завдання 1');
const a = ['a', 'b', 'c'];
const b = [1, 2, 3];
document.write('<p>Масив а: ' + a);
document.write('<p>Масив b: ' + b);
c = a + ',' + b;
document.write('<p>Масив c: ' + c);
document.write('<hr/>');

//Дан масив ['a', 'b', 'c']. Додайте йому до кінця елементи 1, 2, 3.
document.write('<p>Завдання 2');
const stack = ['a', 'b', 'c'];
document.write('<p>Вихідний масив: ' + stack.join(' '));
const lenght = stack.push(1, 2, 3);
document.write("<p>Після додавання: " + stack.join(" "));
document.write('<hr/>');

//Дан масив [1, 2, 3]. Зробіть із нього масив [3, 2, 1].
document.write('<p>Завдання 3');
let d = [1, 2, 3];
document.write('<p>Вихідний масив: ' + d.join(' '));
d.reverse();
document.write('<p>Масив після зміни напрямку: ' + d.join(' '));
document.write('<hr/>');

//Дан масив [1, 2, 3]. Додайте йому до кінця елементи 4, 5, 6.
document.write('<p>Завдання 4');
const vik = [1, 2, 3];
document.write('<p>Вихідний масив: ' + vik.join(' '));
const vikA = vik.push(4, 5, 6);
document.write('<p>Після додавання: ' + vik.join(' '));
document.write('<hr/>');

//Дан масив [1, 2, 3]. Додайте йому на початок елементи 4, 5, 6.
document.write('<p>Завдання 5');
const sv = [1, 2, 3];
document.write('<p>Вихідний масив: ' + sv.join(' '));
sv.unshift(4, 5, 6);
document.write('<p>Після додавання: ' + sv.join(' '));
document.write('<hr/>');

//Дан масив ['js', 'css', 'jq']. Виведіть перший елемент на екран.
document.write('<p>Завдання 6');
const vs = ['js', 'css', 'jq'];
document.write('<p>Вихідний масив: ' + vs.join(' '));
var value = vs.shift();
document.write("<p>Перщий елемент: " + value);
document.write('<hr/>');

//Дан масив [1, 2, 3, 4, 5]. За допомогою методу slice запишіть нові елементи [1, 2, 3].
document.write('<p>Завдання 7');
const vika = [1, 2, 3, 4, 5];
document.write('<p>Вихідний масив: ' + vika.join(' '));
let temp = vika.slice(0, 3);
document.write('<p>Нові елементи: ' + temp.join(' '));
document.write('<hr/>');

//Дан масив [1, 2, 3, 4, 5]. За допомогою методу splice перетворіть масив на [1, 4, 5].
document.write('<p>Завдання 8');
const e = [1, 2, 3, 4, 5];
document.write('<p>Вихідний масив: ' + e.join(' '));
e.splice(1, 2);
document.write('<p>Новий масив: ' + e.join(' '));
document.write('<hr/>');

//Дан масив [1, 2, 3, 4, 5]. За допомогою методу splice перетворіть масив на [1, 2, 10, 3, 4, 5].
document.write('<p>Завдання 9');
const v = [1, 2, 3, 4, 5];
document.write('<p>Вихідний масив: ' + v.join(' '));
v.splice(2, 0, 10);
document.write('<p>Новий масив: ' + v.join(" "));
document.write('<hr/>');

//Дан масив [3, 4, 1, 2, 7]. Відсортуйте його.
document.write('<p>Завдання 10');
let f = [3, 4, 1, 2, 7];
document.write('<p>Вихідний масив: ' + f.join(' '));
f.sort();
document.write('<p>Відсортований масив: ' + f.join(' '));
document.write('<hr/>');

//Дан масив з елементами 'Привіт, ', 'світ' і '!'. Потрібно вивести на екран фразу 'Привіт, мир!'.
document.write('<p>Завдання 11');
let w = ['Привіт, ', 'світ', '!'];
document.write('<p>Вихідний масив: ' + w.join(' '));
w.splice(1, 1, 'мир');
document.write('<p>' + w.join(''));
document.write('<hr/>');

//Дан масив ['Привіт, ', 'світ', '!']. Необхідно записати в нульовий елемент цього масиву слово 'Поки, ' (тобто замість слова 'Привіт, ' буде 'Поки, ').
document.write('<p>Завдання 12');
let p = ['Привіт, ', 'світ', '!'];
document.write('<p>Вихідний масив: ' + p.join(' '));
p.splice(0, 1, 'Поки, ');
document.write('<p>' + p.join(''));
document.write('<hr/>');

//Створіть масив arr з елементами 1, 2, 3, 4, 5 двома різними способами.
document.write('<p>Завдання 13');
let g = [1, 2, 3, 4, 5];
document.write('<p>Масив створений за допомогою []: ' + g.join(' '));
let h = new Array(5);
h[0] = 1;
h[1] = 2;
h[2] = 3;
h[3] = 4;
h[4] = 5;
document.write('<p>Масив створений за допомогою конструктора Array: ' + h.join(' '));
document.write('<hr/>');

/*Дан багатовимірний масив arr:
<br>
<pre>
    <code>
        var arr = [
            ['блакитний', 'червоний', 'зелений'],
            ['blue', 'red', 'green'],
        ];
    </code>
</pre>
Виведіть за його допомогою слово 'блакитний' 'blue' .*/
document.write('<p>Завдання 14');
document.write('<p>Вихідний масив: ')
var arr = [
    ['блакитний', 'червоний', 'зелений'],
    ['blue', 'red', 'green'],
];
document.write("<br/>")
for (var i = 0; i < 2; i++) {
    for (var j = 0; j < 3; j++)
        document.write(arr[i][j] + " ")
    document.write("<br>")
};
document.write('<p>Виведені елементи: ')
document.write(arr[0][0] + ' ');
document.write(arr[1][0]);
document.write('<hr/>');
//Створіть масив arr = ['a', 'b', 'c', 'd'] і за допомогою його виведіть на екран рядок 'a+b, c+d'.
document.write('<p>Завдання 15');
let n = ['a', 'b', 'c', 'd'];
document.write('<p>Вихідний масив: ' + n.join(' '));
j = n.splice(2, 2);

document.write('<p>Виведений рядок: ' + n.join('+') + ', ' + j.join('+'));
document.write('<hr/>');
/*Запитайте у користувача кількість елементів масиву. Виходячи з даних, які ввів користувач
        створіть масив на ту кількість елементів, яку передав користувач.
        у кожному індексі масиву зберігайте чило який показуватиме номер елемента масиву.*/
document.write('<p>Завдання 16');
let A = +prompt("Введіть число для завдання 16");
let B = [];
document.write('<p>Масив: ')

for (i=0; i < A; i++) {
    B[i] = i;
    document.write(B[i]+ ' '); 
  
}
document.write('<hr/>');
/*Зробіть так, щоб з масиву, який ви створили вище, вивелися всі непарні числа в параграфі, а парні в спані з червоним тлом.*/
document.write('<p>Завдання 17');
document.write('<p>Новий масив: ');
document.write("<br>");
for (i=0; i < A; i++) {
    B[i] = i;
    document.write(B[i]+ ' '); {
        if ((i) % 2 == 1) 
  document.write(B[i] = '<p>')
  else  
  document.write(B[i] = `<span style='background-color:red;'>`) 
    }
}
document.write('<hr/>');
/*Напишіть код, який перетворює та об'єднує всі елементи масиву в одне рядкове значення. Елементи масиву будуть розділені комою.
           <pre>
               <code>
                var vegetables = ['Капуста', 'Ріпа', 'Редиска', 'Морковка'];
    
                <span class="green">// Ваш код</span>

                
                document.write(str1); // "Капуста, Ріпа, Редиска, Морквина"
               </code>
           </pre>*/
document.write('<p>Завдання 18');
var vegetables = ['Капуста', 'Ріпа', 'Редиска', 'Морковка'];
var str1 = vegetables.join(', ');
document.write('<p>Рядкове значення: <br/>' + str1);



 



















