/*Функція ggg приймає 2 параметри: анонімну функцію, яка повертає 3 та анонімну функцію, яка
повертає 4. Поверніть результатом функції ggg суму 3 та 4.*/

const fun = {
    one: 3,
    two: 4,

    tree: function (){
        return(fun.one + fun.two); 
    },

    four: function (){
        console.log(fun.tree ())
    }
};

fun.four();



