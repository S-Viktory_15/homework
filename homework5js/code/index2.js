/*Створіть об'єкт криптокошилок. У гаманці має зберігатись ім'я власника, кілька валют Bitcoin, 
Ethereum Stellar і в кожній валюті додатково є ім'я валюти, логотип, кілька монет та курс на сьогоднішній день.
 Також в об'єкті гаманець є метод при виклику якого він приймає ім'я валюти та виводить на сторінку  інформацію.
 "Доброго дня, ... ! На вашому балансі (Назва валюти та логотип) залишилося N монет, якщо ви сьогодні продасте
  їх те, отримаєте ...грн.*/

  const wallet = {
    name: "Vika",
    btc: {
        name: "Bitcoin",
        logo: "<img src = 'https://i-invdn-com.investing.com/ico_flags/80x80/v32/bitcoin.png'>",
        coin: "50",
        rate: "29881.2 "

    },
    eth: {
        name: "Ethereum",
        logo: "<img src = 'https://i-invdn-com.investing.com/ico_flags/80x80/v32/ethereum.png'>",
        coin: "100",
        rate: "2090.96" 
    },
    xlm: {
        name: "Stellar",
        logo: "<img src = 'https://i-invdn-com.investing.com/ico_flags/80x80/v32/stellar.png'>",
        coin: "150",
        rate: "0.10567" 
    },

    show: function (nameCoin) {
    document.getElementById("wallet").innerHTML = `Доброго дня, ${wallet.name}! На вашому балансі ${wallet[nameCoin].name} ${wallet[nameCoin].logo} залишилося ${wallet[nameCoin].coin} монет, якщо ви сьогодні продасте
       їх те, отримаєте ${wallet[nameCoin].coin*wallet[nameCoin].rate*38} грн.`    }

  }
  wallet.show (prompt("Введіть назви монет: ", "btc, eth, xlm"));
