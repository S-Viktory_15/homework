/*Реалізуйте клас Worker (Працівник), який матиме такі властивості: name (ім'я), surname (прізвище),
rate (ставка за день роботи), days (кількість відпрацьованих днів).
Також клас повинен мати метод getSalary(), який виводитиме зарплату працівника.
Зарплата - це добуток (множення) ставки rate на кількість відпрацьованих днів days.
*/

function Worker(name, surname, rate, days) {
    this.name = name,
    this.surname = surname,
    this.rate = rate,
    this.days = days,

    this.getSalary = function () {
        document.getElementById("woker").innerHTML += `Заробітна плата праціника ${this.name} ${this.surname} - ${this.rate*this.days} грн. <br />`
    }


}

const A = new Worker("Vika", "Slobodian", "1000", "25");
const B = new Worker("Vova", "Bondarenko", "1200", "20");
const C = new Worker("Tanya", "Lugova", "2000", "25");

A.getSalary();
B.getSalary();
C.getSalary();





