/* Створіть програму секундомір.
            <br>
            * Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"<br>
            * При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий
            * Виведення лічильників у форматі ЧЧ:ММ:СС<br>
            * Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції*/
window.onload = () => {
    const seconds = document.getElementById("seconds"),
        minute = document.getElementById("minute"),
        hour = document.getElementById("hour"),
        start = document.getElementById("Start"),
        stop = document.getElementById("Stop"),
        reset = document.getElementById("Reset"),
        container = document.getElementById("div");

    let counter = 0;
    minut = 1,
    num = 1,
    flag = false;

    let timer;



    const buttonHandler1 = () => {
        container.classList.remove("black");
        container.classList.remove("silver");
        container.classList.remove("red");
        container.classList.add("green");

    };

    const buttonHandler2 = () => {
        container.classList.remove("black");
        container.classList.remove("green");
        container.classList.remove("silver");
        container.classList.add("red");

    };
    const buttonHandler3 = () => {
        container.classList.remove("black");
        container.classList.remove("green");
        container.classList.remove("red");
        container.classList.add("silver");

    };

const count = () => {
            counter++;
            
            seconds.textContent = ("0"+counter).slice(-2);
           if(counter ===59){
            counter = 0;
           
            minute.textContent = ("0"+minut++).slice(-2);
           };
           if(minut === 60){
            minut = 0;
            hour.textContent = ("0"+num++).slice(-2);
           }
           if(num ===24){
            num = 0;
           }


        };


    start.onclick = () => {
        if(!flag){
            timer = setInterval(count, 1000);
            flag = true;
        }

        buttonHandler1();
    };
    stop.onclick = () => {
        clearTimeout(timer);
        flag = false;


        buttonHandler2();
    };
    reset.onclick = () => {
        seconds.textContent = ("00");
        minute.textContent = ("00");
        hour.textContent = ("00");
        clearTimeout(timer);
        counter = 0;
        flag = false;
        buttonHandler3();
    };


}