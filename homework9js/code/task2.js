/*Реалізуйте програму перевірки телефону<br>
            * Використовуючи JS Створіть поле для введення телефону та кнопку збереження<br>
            * Користувач повинен ввести номер телефону у форматі 0(Початок з 0)ХХ-ХХХ-ХХ-ХХ <br>

            * Після того як користувач натискає кнопку зберегти перевірте правильність введення номера,
 якщо номер правильний
            зробіть зелене тло і використовуючи document.location переведіть через 3с користувача на сторінку
            https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg
            якщо буде помилка, відобразіть її в діві до input.*/

window.onload = function() {
const body = document.querySelector("body");
const div = document.createElement("div");
body.append(div);
const tel = document.createElement("input");
tel.type = "tel";
tel.placeholder = "0XX-XXX-XX-XX";

div.append(tel);

const paragraf = document.createElement("p");
div.append(paragraf);
const button = document.createElement("button");
button.type = "button";
button.textContent = "Зберегти";
paragraf.append(button);
const error = document.createElement("div");


button.onclick = function(){
const pattern = /^0\d{2}-\d{3}-\d{2}-\d{2}/;
const test = pattern.test(tel.value);

if(test ===false) {
    
    div.prepend(error);
    error.textContent = "Номер телефону введений не корректно, повторіть спробу.";
    
    
}
else{
    
    body.style.backgroundColor = "green";
    setTimeout(function () {
        document.location = "https://www.shutterstock.com/image-photo/highland-mediterranean-travel-landscape-scenic-600w-2224233313.jpg"
    }, 3000);
    
    
error.remove();
}

 
console.log(test);
}








}
